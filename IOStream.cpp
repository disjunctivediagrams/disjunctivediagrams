﻿#include "IOStream.h"
#include "DimacsParser.h"

namespace IOStream
{

Log::Log(const std::string& filename)
{
	out_.open(filename.c_str(), std::ios::out);
	if(!out_.is_open())
	{
		throw std::runtime_error(std::string("Can't open log: ") + filename);
	}
}

Log::Log(const char* filename)
{
	out_.open(filename, std::ios::out);
	if(!out_.is_open())
	{
		throw std::runtime_error(std::string("Can't open log: ") + std::string(filename));
	}
}

Log::~Log()
{
	if(out_.is_open())
		out_.close();
}

void Log::WriteOut(const char* msg)
{
	out_ << msg << std::endl;
}

void Log::WriteOut(const std::string& msg)
{
	out_ << msg << std::endl;
}

void Log::WriteOut(const Problem& p)
{
	//! Посчитаем всю информацию для заголовка
	{
		std::set<VarId> var_set;
		size_t lit_count = 0;
		for(unsigned i = 0; i < p.size(); i++)
		{
			const Clause& c = p[i];
			lit_count += c.size();
			for(unsigned j = 0; j < c.size(); j++)
			{
				const Lit& lit = c[j];
				var_set.insert(static_cast<VarId>(lit >> 1));
			}
		}

		out_ << "p cnf " << var_set.size() << " " << p.size() << std::endl;
		out_ << "c literals_count " << lit_count << std::endl;
		out_ << "c min_var_num " << *(var_set.begin()) << std::endl;
		out_ << "c max_var_num " << *(var_set.rbegin()) << std::endl;
	}

	//! Теперь записываем формулу в файл
	for(unsigned i = 0; i < p.size(); i++)
	{
		const Clause& c = p[i];
		for(unsigned j = 0; j < c.size(); j++)
		{
			const Lit& lit = c[j];
			const VarId var_id = (lit >> 1);
			out_ << (lit & 1 ? "-" : "") << var_id << " ";
		}
		out_ << "0\n";
	}
}

void Log::WriteOut(const VarOrder& order)
{
	for(unsigned i = 0; i < order.size(); i++)
	{
		if(order[i] > 0)
		{
			out_ << "VarId = " << i << ", order = " << order[i] << std::endl;
		}
	}
}

void Log::WriteOut(const EquationSystem& sys)
{
	for(EquationSystem::Equations::const_iterator eq_it = sys.equations_.begin();
		eq_it != sys.equations_.end();
		eq_it++)
	{
		const EquationSystem::Equation& eq = *eq_it;
		out_ << eq.first << " = ";
		const EquationSystem::VarDisj& disj = eq.second;
		for(unsigned i = 0; i < disj.size(); i++)
		{
			const EquationSystem::VarConj& conj = disj[i];
			out_ << (i==0?"":" | ") << "x" << conj.fake_var_id << " & " << "x" << conj.node_var_id;
		}
		out_ << std::endl;
	}
}

void Log::WriteOut(const DiagramStatistic& stat)
{
	out_ << stat << std::endl;
}

void Log::WriteOut(const EquationSystemStatistic& stat)
{
	out_ << stat << std::endl;
}

void ReadProblem(const std::string& filename, Problem& problem, VarOrder& order)
{
	std::ifstream file(filename.c_str(), std::ios::in);
	if(!file.is_open())
	{
		throw std::runtime_error(std::string("Can't open file ") + filename);
	}

	IStream in(file);
	if(!parseDimacs(in, problem, order))
	{
		file.close();
		throw std::runtime_error("Parse the input file is failed!");
	}
	file.close();
}

void ReadLinesFromFile(const std::string& filename, std::vector<std::string>& lines)
{
	std::ifstream file(filename.c_str(), std::ios::in);
	if(!file.is_open())
	{
		throw std::runtime_error(std::string("Can't open file ") + filename);
	}

	lines.clear();
	while(file.good() && !file.eof())
	{
		std::string line;
		std::getline(file, line);
		if(!line.empty())
		{
			lines.push_back(line);
		}
	}

	file.close();
}

} // namespace IOStream
