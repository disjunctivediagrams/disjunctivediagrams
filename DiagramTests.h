#ifndef DIAGRAM_TESTS_H
#define DIAGRAM_TESTS_H

#include "Types.h"

class DiagramTests
{
public:
	DiagramTests(){}

	void AddTest(const std::string& testfile, const std::string& diagram_log, ProblemType problem_type);

	void RunAllTest(std::ostream& out);

private:

	// to do: ������� �������� TestDescriptor, � ������� ����� ��� �������� ��� ������ �����
	typedef std::vector<std::string> FilenameContainer;
	FilenameContainer tests_;
	FilenameContainer diagram_logs_;
	std::vector<ProblemType>  problem_types_;
};


#endif // DIAGRAM_TESTS_H

