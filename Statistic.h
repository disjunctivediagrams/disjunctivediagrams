#pragma once
#include "Types.h"

//! ���������� ��������� �������������� ���������� �� ���������
//! ����� ����� ����� ����������� �������� ��������� �� ������������
// fixme: ����������� ���������� ���� �����
class DiagramStatistic
{
public:
	DiagramStatistic(const DisjunctiveDiagram& diagram);

	unsigned AvgParentCount() const {return avg_parent_count_;}
	unsigned AvgHighChildsCount() const {return avg_high_childs_count_;}
	unsigned AvgLowChildsCount() const {return avg_low_childs_count_;}
	unsigned MaxParentCount() const {return max_parent_count_;}
	unsigned MaxHighChildsCount() const {return max_high_childs_count_;}
	unsigned MaxLowChildsCount() const {return max_low_childs_count_;}
	unsigned MinParentCount() const {return min_parent_count_;}
	unsigned MinHighChildsCount() const {return min_high_childs_count_;}
	unsigned MinLowChildsCount() const {return min_low_childs_count_;}

	unsigned VertexCount() const {return vertex_count_;}
	unsigned LeafCount() const {return leaf_count_;}
	unsigned RootCount() const {return root_count_;}
	unsigned InnerVertexCount() const {return inner_vertex_count_;}
	size_t MemorySize() const {return memory_size_;}
	unsigned VariableCount() const {return var_count_;}
	VarId MinVarId() const {return min_var_id_;}
	VarId MaxVarId() const {return max_var_id_;}
	unsigned TruePathCount() const {return true_path_count_;}
	unsigned FalsePathCount() const {return false_path_count_;}
	unsigned MaxPathDepth() const {max_path_depth_;}

	friend std::ostream& operator<<(std::ostream& out, const DiagramStatistic& stat);

private:

	//! �������������� ���������� �� ��������� �������
	struct VertexInfo
	{
		VertexInfo(unsigned p = 0, unsigned h = 0, unsigned l = 0) 
			: parent_count(p)
			, high_childs_count(h)
			, low_childs_count(l)
		{}
		unsigned parent_count;
		unsigned high_childs_count;
		unsigned low_childs_count;
	};

	typedef std::map<VertexId, VertexInfo> VertexInfoMap;

	void CalcParentChildCounts(const DiagramNodes& roots, VertexInfoMap& vertex_map);
	void IncrementParentCount(const DiagramNodes& nodes, VertexInfoMap& vertex_map);

private:
	unsigned memory_size_;

	unsigned vertex_count_;
	unsigned leaf_count_;
	unsigned root_count_;
	unsigned inner_vertex_count_;
	unsigned var_count_;
	unsigned min_var_id_;
	unsigned max_var_id_;
	unsigned true_path_count_;
	unsigned false_path_count_;
	unsigned max_path_depth_;

	unsigned avg_parent_count_;
	unsigned avg_high_childs_count_;
	unsigned avg_low_childs_count_;
	unsigned max_parent_count_;
	unsigned max_high_childs_count_;
	unsigned max_low_childs_count_;
	unsigned min_parent_count_;
	unsigned min_high_childs_count_;
	unsigned min_low_childs_count_;

};

class EquationSystemStatistic
{
public:
	EquationSystemStatistic(const EquationSystem& sys);

	unsigned EquationCount() const {return equations_count_;}
	unsigned LiteralCount() const {return literals_count_;}
	unsigned MinEquationLength() const {return min_equation_len_;}
	unsigned MaxEquationLength() const {return max_equation_len_;}
	unsigned AvgEquationLength() const {return avg_equation_len_;}

	friend std::ostream& operator<<(std::ostream& out, const EquationSystemStatistic& stat);

private:
	unsigned equations_count_;
	unsigned literals_count_;
	unsigned min_equation_len_;
	unsigned max_equation_len_;
	unsigned avg_equation_len_;
};
