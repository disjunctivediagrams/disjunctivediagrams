﻿/*
 * hash.h
 *
 *  Created on: Apr 30, 2007
 *      Author: Alexey S. Ignatiev
 *      E-mail: twosev@gmail.com
 */

#ifndef HASH_H_
#define HASH_H_

// system includes:
// local includes:
//#include "uint64vect.h"
// local defines:
// local types:

#define bitwise_rotate(x, k) (((x) << (k)) | ((x) >> (32 - (k))))

#define hash_bob_mix(a, b, c) {                                               \
	a -= c;  a ^= bitwise_rotate(c,  4);  c += b;                         \
	b -= a;  b ^= bitwise_rotate(a,  6);  a += c;                         \
	c -= b;  c ^= bitwise_rotate(b,  8);  b += a;                         \
	a -= c;  a ^= bitwise_rotate(c, 16);  c += b;                         \
	b -= a;  b ^= bitwise_rotate(a, 19);  a += c;                         \
	c -= b;  c ^= bitwise_rotate(b,  4);  b += a; }

#define hash_bob_final(a, b, c) {                                             \
	c ^= b; c -= bitwise_rotate(b, 14);                                   \
	a ^= c; a -= bitwise_rotate(c, 11);                                   \
	b ^= a; b -= bitwise_rotate(a, 25);                                   \
	c ^= b; c -= bitwise_rotate(b, 16);                                   \
	a ^= c; a -= bitwise_rotate(c,  4);                                   \
	b ^= a; b -= bitwise_rotate(a, 14);                                   \
	c ^= b; c -= bitwise_rotate(b, 24); }

//
//=============================================================================
static inline unsigned hash_bob_lookup3(const unsigned *key, unsigned length)
//
// A hash function by Bob Jenkins (see http://burtleburtle.net/bob/c/lookup3.c)
//-----------------------------------------------------------------------------
{
	unsigned a, b, c;

	// set up the internal state
	a = b = c = 0xdeadbeef + (length << 2);

	// handle most of the key
	while (length > 3) {
		a += key[0];
		b += key[1];
		c += key[2];
		hash_bob_mix(a, b, c);
		length -= 3;
		key    += 3;
	}

	// handle the last 3 unsigned ints
	switch (length) {
	case 3:
		c += key[2];
	case 2:
		b += key[1];
	case 1:
		a += key[0];
		hash_bob_final(a, b, c);
	case 0:
		// case 0: nothing left to add
		break;
	}

	return c;
}
/*
//
//=============================================================================
static inline unsigned minexpo(unsigned number)
//
// Finds a minimum number 2^p which is larger then a given number.
//-----------------------------------------------------------------------------
{
	if ((number & (number - 1)) == 0)
		return number;

	return 1 << (uint64_highest_bit_pos(number) + 1);
}
*/
#endif /* HASH_H_ */
