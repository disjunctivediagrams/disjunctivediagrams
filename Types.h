﻿#ifndef TYPES_H
#define TYPES_H

#include "stdafx.h"
#include <unordered_set>
#include <unordered_map>

// Литерал
typedef unsigned int Lit;
typedef unsigned int uint;

// Ограничение (Конъюнкт / Дизьюнкт)
typedef std::vector<Lit> Clause;

// Формула (ДНФ / КНФ)
typedef std::vector<Clause> Problem;

// Указатель (итератор) на литерал в ограничениее
typedef Clause::iterator LitPtr;  

// Указатель (итератор) на ограничение в формуле
typedef Problem::iterator ClausePtr;

// Тип формулы: КНФ, ДНФ, конфликтная база (тоже КНФ, но нужно отличать)
enum ProblemType
{
	ProblemTypeConflict,
	ProblemTypeCnf,
	ProblemTypeDnf
};

// Тип узла диаграммы
enum DiagramNodeType
{
	Undefined,
	RootNode,
	InternalNode,
	FalseNode,
	TrueNode
};

typedef unsigned VarId;
typedef std::set<VarId> VarSet;
typedef std::vector<VarId> VarContainter;
typedef std::pair<VarId, unsigned> VarCounter;
typedef std::map<VarId, unsigned> VarCounterMap;

//! Сопоставляем VarId (номер переменной - индекс) <--> unsigned (порядок)
typedef std::vector<unsigned> VarOrder;

typedef unsigned VertexId;
typedef size_t HashKey;

class DiagramNode;
typedef DiagramNode* DiagramNodePtr;
typedef std::vector<DiagramNodePtr> DiagramNodes;

//! Бинарный предикат, предназначенный для упорядочивания узлов диаграммы
struct DiagramNodeLess: public std::binary_function<DiagramNodePtr, DiagramNodePtr, bool>
{
	bool operator()(const DiagramNodePtr& node_1, const DiagramNodePtr& node_2) const;
};

//! Проверка узлов диаграммы на равенство - рекурсивно проверяется все поддерево
struct DiagramNodeEquals: public std::binary_function<DiagramNodePtr, DiagramNodePtr, bool>
{
	bool operator()(const DiagramNodePtr& node_1, const DiagramNodePtr& node_2) const;
};

//! Унарный предикат - возвращает хеш-значение для узла диаграммы
struct DiagramNodeHash: public std::unary_function<DiagramNodePtr, HashKey>
{
	HashKey operator()(const DiagramNodePtr& node) const;
};

//! Упорядоченное множество уникальных узлов
//! Используется для представления множеств high/low-потомков
typedef std::unordered_set<DiagramNodePtr, DiagramNodeHash, DiagramNodeLess> DiagramNodesSet;

//! Вершина диаграммы.
//! Все вершины диаграммы пронумерованы (поле vertex_id).
//! С каждой вершиной связана некоторая переменная исходной формулы (поле var_id) и список потомков.
class DiagramNode
{
public:

	static unsigned constructors_;
	static unsigned destructors_;

	DiagramNode(DiagramNodeType type = Undefined);
	DiagramNode(DiagramNodeType type, VarId var);
	DiagramNode(DiagramNodeType type, VarId var, const DiagramNodesSet& high, const DiagramNodesSet& low);

	~DiagramNode();

	//! Тип вершины
	DiagramNodeType node_type;
	//! Номер переменной (нумерация с единицы)
	VarId var_id;
	//! Дочерние узлы
	DiagramNodes high_nodes;
	DiagramNodes low_nodes;

	//! Номер фиктивной переменной, ассоциированной с этой вершиной
	VarId markup_var_id;
	//! Номер вершины (нумерация с единицы)
	VertexId vertex_id;
	//! Хеш-значение данного узла
	HashKey hash_key;

	bool IsLeaf() const {return node_type == FalseNode || node_type == TrueNode;}
	bool IsRoot() const {return node_type == RootNode;}

	//! 0 - для FalseNode
	//! 1 - для TrueNode
	//! удвоенный номер вершины -  для остальных узлов
	unsigned Value() const {return (var_id << 1) | (node_type == TrueNode ? 1 : 0);}

	bool Equals(const DiagramNode& node) const;
	//! Размер узла диаграммы в байтах
	inline size_t Size() const;
};

//! Хеш-таблица для хранения диаграммы
typedef std::unordered_set<DiagramNodePtr, DiagramNodeHash, DiagramNodeEquals> DiagramHashTable;

//! Дизъюнктивная диаграмма - результат работы DisjunctiveDiagramBuilder
class DisjunctiveDiagram
{
	friend class DisjunctiveDiagramsBuilder;
public:
	DisjunctiveDiagram();
	~DisjunctiveDiagram();

	const DiagramHashTable& GetTable() const {return table_;}
	const DiagramNodes& GetRoots() const {return roots_;}
	ProblemType GetProblemType() const {return problem_type_;}

	DiagramNode* GetTrueLeaf() const {return &true_leaf_;}
	DiagramNode* GetFalseLeaf() const {return &false_leaf_;}

	//! Статистика, собранная при построении диаграммы
	size_t VariableCount() const {return var_set_.size();}
	VarId MinVarId() const {return var_set_.size() ? *(var_set_.begin()) : 0;}
	VarId MaxVarId() const {return var_set_.size() ? *(var_set_.rbegin()) : 0;}
	size_t VertexCount() const {return table_.size();}

	size_t TruePathCount() const {return true_path_count_;}
	size_t FalsePathCount() const {return false_path_count_;}
	size_t MaxPathDepth() const {return max_path_depth_;}
	size_t DuplicateReducedCount() const {return duplicate_reduced_;}
	size_t VertexReducedCount() const {return vertex_reduced_;}
	size_t RootJoinCount() const {return root_join_cnt_;}

	size_t DiagramSize() const;

protected:

	DiagramHashTable table_;
	DiagramNodes roots_;

	VarSet var_set_;

	ProblemType problem_type_;

	// Терминальные вершины (листья)
	static DiagramNode true_leaf_;
	static DiagramNode false_leaf_;

	size_t variable_count_;
	size_t true_path_count_;
	size_t false_path_count_;
	size_t max_path_depth_;
	size_t duplicate_reduced_;
	size_t vertex_reduced_;
	unsigned min_var_num_;
	unsigned max_var_num_;
	size_t root_join_cnt_;
};

typedef std::shared_ptr<DisjunctiveDiagram> DisjunctiveDiagramPtr;

//! Система уравнений в формате y = x_1 & u_1 | ... | x_n & u_n
class EquationSystem
{
public:
	EquationSystem() {} 

	struct VarConj
	{
		VarConj(VarId node_var_id_ = 0, VarId fake_var_id_ = 0)
			: node_var_id(node_var_id_)
			, fake_var_id(fake_var_id_)
		{}
		VarId node_var_id;
		VarId fake_var_id;
	};
	typedef std::vector<VarConj> VarDisj;
	typedef std::pair<VarId, VarDisj> Equation;
	typedef std::map<VarId, VarDisj> Equations;

	Equations equations_;
};

typedef std::shared_ptr<EquationSystem> EquationSystemPtr;

template <class T>
inline void hash_comb(uint64_t& seed, const T& v)
{
	std::hash<T> hasher;
	seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

/**
* \brief Представляет опции командной строки.
*/
struct Options
{
	Options();

	std::string extract_subproblem;

	//! Анализ конфликтных баз, перечисленные в файле, переданном в параметре analyze
	std::string analyze;
	std::string analyze_log;
	size_t analyze_var_limit;
	float analyze_var_fraction;

	std::string dir;
	std::string filename;
	std::string source;
	std::string order;
	std::string table; // fixme: unused
	std::string convert;

	bool run_tests = false;
	bool show_statistic = false;
	bool show_version = false;
	bool show_options = false;
	bool show_help = false;
};

#endif // TYPES_H
