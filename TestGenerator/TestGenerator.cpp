#include "stdafx.h"
#include "GenTrueRandom.h"
#include "../IOStream.h"
#include "../Utils.h"

void WriteOutDimacs(const std::string& filename, const Problem& problem);
void WriteOutDimacs(std::ostream& out, const Problem& problem);

int main(int argc, char* argv[])
{
	namespace po = boost::program_options;
	try
	{
		po::options_description options("Usage: TestGenerator [options]");
		options.add_options()
			("help,h", "display this help and exit")
			("version,v", "output current program version")
			("file,f", po::value<std::string>(), "input template file")
			("rnd,r", po::value<std::string>()->default_value("true_random"), "file contains true random data")
			("count,c", po::value<unsigned>()->default_value(1), "count of tests")
			("in-vars,i", po::value<unsigned>(), "count of input variables")
			("out-vars,o", po::value<unsigned>(), "count of output variables")
			;

		po::variables_map params;
		po::store(po::parse_command_line(argc, argv, options), params);

		if (params.count("help"))
		{
			std::cout << options;
			return 0;
		}

		if (params.count("version"))
		{
			std::cout << "Version: 0.1.0.0" << std::endl;
			std::cout << "Build date: " << __DATE__ << " " << __TIME__ << std::endl;
			return 0;
		}

		const std::string template_file = params["file"].as<std::string>();
		const std::string true_random_file = params["rnd"].as<std::string>();
		const unsigned test_count = params["count"].as<unsigned>();
		const unsigned in_vars = params["in-vars"].as<unsigned>();
		const unsigned out_vars = params["out-vars"].as<unsigned>();

		std::string file_path, file_name, file_suffix;
		{
			std::string tmp;
			SplitFilename(template_file, file_path, tmp);
			SplitFileSuffix(tmp, file_name, file_suffix);
		}

		Problem problem;
		{
			VarOrder order;
			IOStream::ReadProblem(template_file, problem, order);
		}

		GenTrueRandom rnd(true_random_file);
		for(unsigned idx = 0; idx < test_count; ++idx)
		{
			//! ��������� ��������������� ��������� � ���
			{
			//	std::vector<bool> key;
			//	rnd.GetRandomStream(in_vars, key);
			//	for(unsigned i = 0; i < key.size(); ++i)
			//	{
			//		Clause cl;
			//		Lit lit = (i << 1) | (key[i] ? 0 : 1);
			//		cl.push_back(lit);
			//		problem.push_back(cl);
			//	}
			}
			//! ������ ����� �� ������� => ��������������� �������� �����
			{
			}
			
			//! ��������� � ������� ��������� �������� �����
			{
				VarId min_var_id(0), max_var_id(0);
				GetMinMaxVars(problem, min_var_id, max_var_id);
				std::vector<bool> key_stream;
				rnd.GetRandomStream(out_vars, key_stream);
				VarId var_id = max_var_id - out_vars + 1;
				for(unsigned i(0); 
					i < key_stream.size() && var_id <= max_var_id; 
					++i, ++var_id)
				{
					const Lit lit = (var_id << 1) | (key_stream[i] ? 0 : 1);
					Clause cl;
					cl.push_back(lit);
					problem.push_back(cl);
				}
			}
			//! ���������� �������� ������� � ����
			{
				std::stringstream ss;
				ss << file_path << file_name << "/" << file_name << "_test_" << idx << file_suffix;
				const std::string testfile(ss.str());
				WriteOutDimacs(testfile, problem);
				problem.resize(problem.size() - out_vars);
			}
		}
	}
	catch(const std::exception& e)
	{
		std::cerr << "[ERROR]: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}

void WriteOutDimacs(const std::string& filename, const Problem& problem)
{
	std::ofstream out(filename.c_str(), std::ios::out);
	if(!out.is_open())
		throw std::runtime_error("Can't open file " + filename);
	WriteOutDimacs(out, problem);
}

void WriteOutDimacs(std::ostream& out, const Problem& problem)
{
	// header
	{
		VarSet var_set;
		ExtractVarSet(problem, var_set);
		out << "p cnf " << var_set.size() << " " << problem.size() << std::endl;
	}
	for(unsigned i(0); i < problem.size(); ++i)
	{
		const Clause& cl = problem[i];
		for(unsigned j(0); j < cl.size(); ++j)
		{
			const Lit lit = cl[j];
			out << (lit & 1 ? "-" : "") << (lit >> 1);
			out << (j + 1 == cl.size() ? " 0\n" : " ");
		}
	}
}
