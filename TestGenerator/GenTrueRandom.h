
typedef unsigned char byte;
typedef unsigned int word;
typedef unsigned long int dword;
typedef unsigned __int64 qword;

class RandomBits
{
public:
	RandomBits();
	bool GetRandomBit();
	unsigned char GetRandomByte();
	unsigned int GetRandomWord();
	unsigned long int GetRandomDword();
	unsigned __int64 GetRandomQword(){};
private:
	unsigned int v;
};


void PrintByte(byte b);
void PrintWord(word w);
void PrintDword(dword d);
//����� �� ������ 2 ���� ��� 
dword Parity(dword x);


class GenTrueRandom
{
public:

	GenTrueRandom(const std::string& filename);

	bool GetRandomStream(size_t size, std::vector<bool>& stream) const;

private:
	// ����, ���������� ��������� ������
	const std::string m_random_file;
	size_t m_length;
};
