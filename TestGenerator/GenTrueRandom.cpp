#include "stdafx.h"
#include "GenTrueRandom.h"

RandomBits::RandomBits()
{
	srand((unsigned int)time(NULL));
	v = 0;
}

bool RandomBits::GetRandomBit()
{
	v = rand();
	v ^= v>>16;
    v ^= v>>8;
    v ^= v>>4;
    v ^= v>>2;
    v ^= v>>1;
    return (bool)(v&1);
}

unsigned char RandomBits::GetRandomByte()
{
	v = rand();
	v ^= v>>16;
    v ^= v>>8;
    return (unsigned char)(v&0xFF);
}

word RandomBits::GetRandomWord()
{
	word x = 0;
	for(size_t i = 0; i < sizeof(word); i++)
		x |= (word)GetRandomByte()<<i*sizeof(byte);
	return x;
}

dword RandomBits::GetRandomDword()
{
	dword x = 0;
	for(unsigned int i = 0; i < 4; i++){
		x |= (dword)GetRandomByte()<<i*8;
	}
	return x;
}

GenTrueRandom::GenTrueRandom(const std::string& filename)
	: m_random_file(filename)
{
	std::ifstream is;
	is.open(filename.c_str(), std::ios::binary);
	is.seekg(0, std::ios::end);
	m_length = (size_t)is.tellg();
	is.seekg(0, std::ios::beg);
	is.close();
}

bool GenTrueRandom::GetRandomStream(size_t size, std::vector<bool>& stream) const
{
	// � ����� �� ������� ������
	if(size > 8*m_length) return false;

	std::ifstream in(m_random_file.c_str(), std::ios::in | std::ios::binary);
	stream.resize(size);
	if(in.is_open())
	{
		// �������������� ������ �� ��������� ����� � �����
		RandomBits rb;
		word pos = rb.GetRandomWord();
		pos = pos % (m_length - (size/8 + 1));
		in.seekg(pos, std::ios::beg);

		// ������ ������ �� �����, ��������� ������ stream
		size_t i = 0;
		char byte;
		while(i < size)
		{
			in.read(&byte, 1);
			for(size_t j = 0; j < 8 && i < size; j++, i++)
				stream[i] = ( byte&(1<<j) ? true : false );
		}
		in.close();
		return true;
	}
	return false;
}

void PrintByte(byte b)
{
	for(int i = 0; i < 8; i++)
		if(b&(1<<i))
			std::cout<<"1";
		else
			std::cout<<"0";
}
void PrintWord(word w)
{
	for(int i = 0; i < 16; i++)
		if(w&(1<<i))
			std::cout<<"1";
		else
			std::cout<<"0";
}
void PrintDword(dword d)
{
	for(int i = 0; i < 32; i++)
		if(d&(1<<i))
			std::cout<<"1";
		else
			std::cout<<"0";
}
dword Parity(dword x)//����� �� ������ 2 ���� ��� 
{
	x ^= x>>16;
	x ^= x>>8;
	x ^= x>>4;
	x ^= x>>2;
	x ^= x>>1;
	return x&1;
}
