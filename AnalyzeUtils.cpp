﻿#include "AnalyzeUtils.h"
#include "IOStream.h"
#include "DimacsParser.h"
#include "DisjunctiveDiagramsBuilder.h"
#include "Utils.h"
#include <iomanip>

ProblemAnalyzer::ProblemAnalyzer(size_t var_set_size, float var_occurence, const std::string log_name)
	: var_set_size_(var_set_size)
	, var_occurence_(var_occurence)
	, log_(log_name)
{}

void ProblemAnalyzer::Analyze(const std::vector<std::string>& files)
{
	for(unsigned i = 0; i < files.size(); ++i)
	{
		std::cout << "Analyze " << files[i] << std::endl;
		Analyze(files[i]);
	}
	//! В конце лога выводим таблицу со всеми результатами
	log_.WriteTotalResults();
}

void ProblemAnalyzer::Analyze(const std::string& file)
{
	AnalyzeResultWriter::AnalyzeResult result;
	result.src_problem_name = file;
	result.var_set_size = var_set_size_;
	result.var_occurence = var_occurence_;

	//! Выделим подформулу, для которой будем строить диаграмму
	VarOrder order;
	Problem sub_problem;
	{
		//! Исходная формула
		Problem source_problem;
		IOStream::ReadProblem(file, source_problem, order);
		SelectMajorSubset(source_problem, sub_problem, result.var_set_info);
		if(sub_problem.empty())
		{
			log_.WriteOut(result);
			return;
		}
		GetProblemParams(source_problem, result.src_problem);
		GetProblemParams(sub_problem, result.sub_problem);
	}
	FillVarOrder(sub_problem, order, "frequence");

	//! Строим диаграмму для sub_problem
	const double start_build_time = cpuTime();
	DisjunctiveDiagramsBuilder builder(sub_problem, order, ProblemTypeConflict);
	DisjunctiveDiagramPtr diagram = builder.BuildDiagram();

	//! Сохраняем в результатах параметры диаграммы
	result.diag_build_time = cpuTime() - start_build_time;
	result.diag_vertex_count = diagram->VertexCount();
	result.diag_bin_size = diagram->DiagramSize();
	result.diag_lit_vertex_ratio = static_cast<double>(result.sub_problem.lit_count) / result.diag_vertex_count;
	result.diag_bin_compress_ratio = static_cast<double>(result.sub_problem.bin_size) / result.diag_bin_size;

	//! Пишем результат в лог
	log_.WriteOut(result);
}

void ProblemAnalyzer::GetProblemParams(const Problem& problem, AnalyzeResultWriter::ProblemParams& params)
{
	VarSet var_set;
	size_t lit_count = 0;
	for(Problem::const_iterator it = problem.begin();
		it != problem.end();
		++it)
	{
		const Clause& c = *it;
		for(unsigned i = 0; i < c.size(); ++i)
		{
			var_set.insert(c[i] >> 1);
		}
		lit_count += c.size();
	}

	params.var_count = var_set.size();
	params.lit_count = lit_count;
	params.clause_count = problem.size();
	params.bin_size = GetProblemBinarySize(problem);
}

void ProblemAnalyzer::SelectMajorSubset(const Problem& source, Problem& target, std::string& info)
{
	//! Для каждой переменной подсчитываем число ее вхождений в формулу (не важно в какой фазе)
	VarCounterMap var_map;
	ExtractVarCounterMap(source, var_map);

	//! Сортируем переменные по числу вхождений в формулу
	std::vector<VarCounter> var_vec(var_map.begin(), var_map.end());
	std::sort(var_vec.begin(), var_vec.end(), VarCounterLess);

	const size_t lit_count = GetLiteralCount(source);

	//! Выделяем множество самых распространенных переменных
	//! Размер множества фиксирован: major_vars.size() == var_set_size_
	size_t i = 0;
	std::stringstream ss;
	VarSet major_vars;
	for(std::vector<VarCounter>::const_reverse_iterator cit = var_vec.rbegin();
		cit != var_vec.rend();
		++cit, ++i)
	{
		if(i >= var_set_size_) break;
		const VarCounter& vc = *cit;
		ss << "Var_id: " << vc.first << ", count: " << vc.second 
			<< ", ratio: " << static_cast<double>(vc.second) / lit_count << std::endl;
		major_vars.insert(vc.first);
	}
	info = ss.str();

	//! Выделим подформулу
	for(unsigned i = 0; i < source.size(); i++)
	{
		const Clause& c = source[i];
		unsigned occurence_cnt = 0;
		for(unsigned j = 0; j < c.size(); ++j)
		{
			const VarId var_id = c[j] >> 1;
			VarSet::const_iterator var_it = major_vars.find(var_id);
			if(var_it != major_vars.end())
			{
				++occurence_cnt;
			}
		}
		//! число вхождений в процентах от общего числа литералов в конъюнкте
		if( static_cast<float>(occurence_cnt) / c.size() >= var_occurence_ )
		{
			target.push_back(source[i]);
		}
	}
}

AnalyzeResultWriter::AnalyzeResultWriter()
	: result_count_(0)
{}

AnalyzeResultWriter::ProblemParams::ProblemParams()
	: var_count(0)
	, clause_count(0)
	, lit_count(0)
	, bin_size(0)
{}

AnalyzeResultWriter::AnalyzeResult::AnalyzeResult()
	: var_set_size(0)
	, var_occurence(0)
	, diag_vertex_count(0)
	, diag_lit_vertex_ratio(0)
	, diag_bin_size(0)
	, diag_bin_compress_ratio(0)
	, diag_build_time(0)
{}

void AnalyzeResultWriter::Write(std::ostream& out, const AnalyzeResult& result)
{
	total_results_.push_back(result);
	//! Точность после запятой - 2 знака
	out.precision(2);
	out << std::fixed;
	out << std::endl;

	out << "Test #" << ++result_count_ << std::endl 
		<< result.src_problem_name 
		<< ", var_set_size = " << result.var_set_size
		<< ", var_occurence (ratio) = " << result.var_occurence
		<< std::endl << std::endl;

	const ProblemParams& p = result.sub_problem;
	if( !(p.var_count && p.clause_count && p.lit_count && p.bin_size) )
	{
		out << "Empty sub-problem!" << std::endl;
		return;
	}

	//! Информация о множестве переменных, по которым выбирали подформулу
	out << result.var_set_info << std::endl;

	//! Фиксируем ширину полей таблицы
	static const unsigned f1_width = 20;
	static const unsigned f2_width = 6;
	static const unsigned f3_width = 8;
	static const unsigned f4_width = 10;
	static const unsigned f5_width = 13;

	out << std::setw(f1_width) << "| " 
		<< std::setw(f2_width) << "Vars" << " | "
		<< std::setw(f3_width) << "Clauses" << " | "
		<< std::setw(f4_width) << "Literals" << " | "
		<< std::setw(f5_width) << "Size (bytes)" << " |" 
		<< std::endl;

	out << "------------------+--------+----------+------------+---------------+" << std::endl;

	out << std::setw(f1_width) << "Source problem | " 
		<< std::setw(f2_width) << result.src_problem.var_count << " | "
		<< std::setw(f3_width) << result.src_problem.clause_count << " | "
		<< std::setw(f4_width) << result.src_problem.lit_count << " | "
		<< std::setw(f5_width) << result.src_problem.bin_size << " |" 
		<< std::endl;

	out << std::setw(f1_width) << "Subset problem | "
		<< std::setw(f2_width) << result.sub_problem.var_count << " | "
		<< std::setw(f3_width) << result.sub_problem.clause_count << " | "
		<< std::setw(f4_width) << result.sub_problem.lit_count << " | "
		<< std::setw(f5_width) << result.sub_problem.bin_size << " |"
		<< std::endl;

	out << std::setw(f1_width) << "Ratio (percent) | "
		<< std::setw(f2_width) << static_cast<double>(result.sub_problem.var_count * 100) / result.src_problem.var_count << " | "
		<< std::setw(f3_width) << static_cast<double>(result.sub_problem.clause_count * 100) / result.src_problem.clause_count << " | "
		<< std::setw(f4_width) << static_cast<double>(result.sub_problem.lit_count * 100) / result.src_problem.lit_count << " | "
		<< std::setw(f5_width) << static_cast<double>(result.sub_problem.bin_size * 100) / result.src_problem.bin_size << " |"
		<< std::endl;

	//! Параметры диаграммы
	out << std::endl 
		<< "Parameters of the diagram:" << std::endl
		<< "--------------------------" << std::endl;

	static const unsigned title_width = 20;

	out << std::setw(title_width) << "Vertexs: " << result.diag_vertex_count << std::endl;
	out << std::setw(title_width) << "Lit/Vertex ratio: " << result.diag_lit_vertex_ratio << std::endl;
	out << std::setw(title_width) << "Size (bytes): " << result.diag_bin_size << std::endl;
	out << std::setw(title_width) << "Size ratio: " << result.diag_bin_compress_ratio << std::endl;
	out << std::setw(title_width) << "Build time (secs): " << result.diag_build_time << std::endl;
	out << std::endl;
}

void AnalyzeResultWriter::WriteTotalResults(std::ostream& out) const
{
	size_t width[] = {0, 0, 0, 0, 0, 0, 0};
	//! Вычислим требуемую ширину полей таблицы
	for(unsigned i(0); i < total_results_.size(); ++i)
	{
		const AnalyzeResult& result = total_results_[i];
		if(result.src_problem_name.size() > width[0])
			width[0] = result.src_problem_name.size() + 2;
	}
	//! Точность после запятой - 2 знака
	out.precision(2);
	out << std::fixed;
	out << std::endl;
	out << std::endl;
	//! Параметры подформулы 
	{
		std::string title[] = {"Test", "Vars (src/subset/ratio)", "Clauses (src/subset/ratio)", 
			" Literals(src/subset/ratio)", "Size bytes (src/subset/ratio)", "Var set size", "Var occurence"};
		const size_t col_cnt = sizeof(title)/sizeof(*title);
		size_t total_width = 3*col_cnt + width[0];
		for(unsigned i(1); i < col_cnt; ++i)
		{
			width[i] = title[i].size();
			total_width += width[i];
		}

		out << "Subproblem params:" << std::endl;
		// header
		for(unsigned i(0); i < col_cnt; ++i)
		{
			out << std::setw(width[i]) << title[i] << " | ";
		}
		out << std::endl << std::string(total_width, '=') << std::endl;
		// body
		for(unsigned i(0); i < total_results_.size(); ++i)
		{
			const AnalyzeResult& result = total_results_[i];
			out << std::setw(width[0]) << result.src_problem_name << " | ";
			out << std::setw(width[1]) << result.get_var_info() << " | ";
			out << std::setw(width[2]) << result.get_clause_info() << " | ";
			out << std::setw(width[3]) << result.get_lit_info() << " | ";
			out << std::setw(width[4]) << result.get_size_info() << " | ";
			out << std::setw(width[5]) << result.var_set_size << " | ";
			out << std::setw(width[6]) << result.var_occurence << " | ";
			out << std::endl;
		}
		out << std::string(total_width, '=') << std::endl;
	}
	out << std::endl;
	out << std::endl;
	//! Параметры диаграммы
	{
		std::string title[] = {"Test", "Vertexs", "Lit/Vertex ratio", "Size bytes", "Size ratio", "Build time"};
		const size_t col_cnt = sizeof(title)/sizeof(*title);
		size_t total_width = 3*col_cnt + width[0];
		for(unsigned i(1); i < col_cnt; ++i)
		{
			width[i] = title[i].size();
			total_width += width[i];
		}

		out << "Diagram params:" << std::endl;
		// header
		for(unsigned i(0); i < col_cnt; ++i)
		{
			out << std::setw(width[i]) << title[i] << " | ";
		}
		out << std::endl << std::string(total_width, '=') << std::endl;
		// body
		for(unsigned i(0); i < total_results_.size(); ++i)
		{
			const AnalyzeResult& result = total_results_[i];
			out << std::setw(width[0]) << result.src_problem_name << " | ";
			out << std::setw(width[1]) << result.diag_vertex_count << " | ";
			out << std::setw(width[2]) << result.diag_lit_vertex_ratio << " | ";
			out << std::setw(width[3]) << result.diag_bin_size << " | ";
			out << std::setw(width[4]) << result.diag_bin_compress_ratio << " | ";
			out << std::setw(width[5]) << result.diag_build_time << " | ";
			out << std::endl;
		}
		out << std::string(total_width, '=') << std::endl;
	}
}

AnalyzeLogWriter::AnalyzeLogWriter(const char* filename)
{
	out_.open(filename, std::ios::out);
}
AnalyzeLogWriter::AnalyzeLogWriter(const std::string& filename)
{
	out_.open(filename.c_str(), std::ios::out);
}

AnalyzeLogWriter::~AnalyzeLogWriter()
{
	if(out_.is_open())
		out_.close();
}

void AnalyzeLogWriter::WriteOut(const AnalyzeResultWriter::AnalyzeResult& result)
{
	writer_.Write(out_, result);
}

void AnalyzeLogWriter::WriteTotalResults()
{
	writer_.WriteTotalResults(out_);
}


std::string AnalyzeResultWriter::AnalyzeResult::get_var_info() const
{
	std::stringstream str;
	str.precision(2);
	str << std::fixed;
	str << src_problem.var_count << " / " << sub_problem.var_count << " / " 
		<< static_cast<double>(sub_problem.var_count * 100) / src_problem.var_count;
	return str.str();
}

std::string AnalyzeResultWriter::AnalyzeResult::get_clause_info() const
{
	std::stringstream str;
	str.precision(2);
	str << std::fixed;
	str << src_problem.clause_count << " / " << sub_problem.clause_count << " / " 
		<< static_cast<double>(sub_problem.clause_count * 100) / src_problem.clause_count;
	return str.str();
}

std::string AnalyzeResultWriter::AnalyzeResult::get_lit_info() const
{
	std::stringstream str;
	str.precision(2);
	str << std::fixed;
	str << src_problem.lit_count << " / " << sub_problem.lit_count << " / " 
		<< static_cast<double>(sub_problem.lit_count * 100) / src_problem.lit_count;
	return str.str();
}

std::string AnalyzeResultWriter::AnalyzeResult::get_size_info() const
{
	std::stringstream str;
	str.precision(2);
	str << std::fixed;
	str << src_problem.bin_size << " / " << sub_problem.bin_size << " / " 
		<< static_cast<double>(sub_problem.bin_size * 100) / src_problem.bin_size;
	return str.str();
}
