﻿#ifndef DISJUNCTIVE_DIAGRAMS_BUILDER_H
#define DISJUNCTIVE_DIAGRAMS_BUILDER_H

#include "Types.h"

//! Будем передавать пары итераторов - (начальный, конечный)
typedef std::pair<Clause::iterator, Clause::iterator> ClauseRange;
typedef std::vector<ClauseRange> ProblemRange;

class DisjunctiveDiagramsBuilder
{
public:

	DisjunctiveDiagramsBuilder(Problem& dnf, const VarOrder& order, ProblemType problem_type);
	~DisjunctiveDiagramsBuilder();

	DisjunctiveDiagramPtr BuildDiagram();

private:
	
	//! Построить дизъюнктивное дерево по (фрагменту) ДНФ (рекурсивно)
	DiagramNodesSet BuildDiagramNodes(ProblemRange& ranges);

	//! Добавляет узел в таблицу и склеить, если уже есть узел с таким же ключом
	//! Возвращает указатель на этот же узел или на результат склеивания
	DiagramNodePtr AddDiagramNode(DiagramNodePtr node);

	//! Перенумерация вершин диаграммы
	void EnumerateDiagramNodes();

private:

	//! Исходная ДНФ, по которой нужно построить диаграмму
	Problem& problem_;
	ProblemType problem_type_;

	//! Порядок переменных (сортируем формулу относительно данного порядка)
	const VarOrder& order_;

	//! Дизъюнктивная диаграмма
	DisjunctiveDiagramPtr diagram_;

	struct LitLess
	{
		LitLess(const VarOrder& order): order_(order){}
		bool operator()(const Lit& a, const Lit& b) const
		{
			return order_[a >> 1] < order_[b >> 1];
		}
		const VarOrder& order_;
	};

	struct ClauseLess
	{
		ClauseLess(const VarOrder& order): order_(order){}
		bool operator()(const Clause& ca, const Clause& cb) const
		{
			const Lit& a = ca[0];
			const Lit& b = cb[0];
			VarId a_id = a >> 1;
			VarId b_id = b >> 1;
			return (a_id == b_id ? a < b : order_[a_id] < order_[b_id]);
		}
		const VarOrder& order_;
	};

	const LitLess lit_less_;
	const ClauseLess clause_less_;
	const DiagramNodeLess diagram_node_less_;
	const DiagramNodeHash hash_;

};

#endif // DISJUNCTIVE_DIAGRAMS_BUILDER_H
