﻿#pragma once

#include "Types.h"

//! Класс, обеспечивающий форматированную запись результата в поток
class AnalyzeResultWriter
{
public:

	struct ProblemParams
	{
		ProblemParams();

		//! Параметры формулы
		size_t var_count;
		size_t clause_count;
		size_t lit_count;
		size_t bin_size;
	};

	struct AnalyzeResult
	{
		AnalyzeResult();

		//! Общая информация о тесте
		std::string src_problem_name;
		size_t var_set_size;
		float var_occurence;

		//! Список основных переменных
		std::string var_set_info;

		//! Параметры исходной формулы
		ProblemParams src_problem;
		//! Параметры выделенной подформулы
		ProblemParams sub_problem;
		//! Параметры диаграммы, построенной по подформуле
		size_t diag_vertex_count;
		double diag_lit_vertex_ratio;
		size_t diag_bin_size;
		double diag_bin_compress_ratio;
		double diag_build_time;

		std::string get_var_info() const;
		std::string get_clause_info() const;
		std::string get_lit_info() const;
		std::string get_size_info() const;
	};

	typedef std::vector<AnalyzeResult> AnalyzeResultContainer;

	AnalyzeResultWriter();

	void Write(std::ostream& out, const AnalyzeResult& result);
	void WriteTotalResults(std::ostream& out) const;

protected:

	//! Счетчик логгированных результатов
	unsigned result_count_;
	AnalyzeResultContainer total_results_;
};

//! Класс, обеспечивающий запись результатов в файл журнала
class AnalyzeLogWriter
{
public:
	AnalyzeLogWriter(const char* filename);
	AnalyzeLogWriter(const std::string& filename);
	~AnalyzeLogWriter();

	void WriteOut(const AnalyzeResultWriter::AnalyzeResult& result);
	void WriteTotalResults();

protected:

	AnalyzeResultWriter writer_;
	std::ofstream out_;
};

//! Класс, осуществляющий анализ исходной проблемы
//! Каждому объекту ProblemAnalyzer сопоставляется единственный файл журнала,
//! который открывает при создании объекта и закрывается при его уничтожении.

class ProblemAnalyzer
{
public:

	ProblemAnalyzer(size_t var_set_size, float var_occurence, const std::string log_name);

	void Analyze(const std::vector<std::string>& files);
	void Analyze(const std::string& file);

	void SelectMajorSubset(const Problem& source, Problem& target, std::string& info);

private:

	static inline bool VarCounterLess(const VarCounter& va, const VarCounter& vb)
	{
		return va.second < vb.second;
	}

	void GetProblemParams(const Problem& problem, AnalyzeResultWriter::ProblemParams& params);

private:

	AnalyzeLogWriter log_;
	const size_t var_set_size_;
	const float var_occurence_;
};
