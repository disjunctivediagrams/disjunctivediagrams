﻿#include "Utils.h"

//! Проверяем существование файла
bool FileExists(const std::string& filename)
{
	std::fstream fin(filename.c_str(), std::ios::in);
	if(fin.is_open())
	{
		fin.close();
		return true;
	}
	return false;
}

//! Извлечь множество уникальных переменных
void ExtractVarSet(const Problem& problem, VarSet& var_set)
{
	var_set.clear();
	for(Problem::const_iterator it = problem.begin(); it != problem.end(); ++it)
	{
		const Clause& c = *it;
		for(Clause::const_iterator cl_it = c.begin(); cl_it != c.end(); ++cl_it)
		{
			const Lit& lit = *cl_it;
			var_set.insert(static_cast<VarId>(lit >> 1));
		}
	}
}

void ExtractVarCounterMap(const Problem& problem, VarCounterMap& var_map)
{
	var_map.clear();
	for(Problem::const_iterator it = problem.begin(); it != problem.end(); ++it)
	{
		const Clause& c = *it;
		for(Clause::const_iterator cl_it = c.begin(); cl_it != c.end(); ++cl_it)
		{
			const VarId var_id = *cl_it >> 1;
			VarCounterMap::iterator it = var_map.find(var_id);
			if(it == var_map.end())
			{
				var_map.insert(VarCounter(var_id, 1));
			}
			else
			{
				it->second++;
			}
		}
	}
}

//! Минимальный и максимальный номер переменной
void GetMinMaxVars(const Problem& problem, VarId& min_var_id, VarId& max_var_id)
{
	VarSet var_set;
	ExtractVarSet(problem, var_set);
	min_var_id = *(var_set.begin());
	max_var_id = *(var_set.rbegin());
}

void SplitFilename(const std::string& path, std::string& dir, std::string& filename)
{
	const auto slash_pos = path.find_last_of("/\\");
	if(slash_pos != std::string::npos)
	{
		const auto dir_len = slash_pos + 1;
		dir = path.substr(0, dir_len);
		filename = (path.size() > dir_len ? path.substr(dir_len) : "");
	}
	else
	{
		dir.clear();
		filename = path;
	}
}

void SplitFileSuffix(const std::string& filename, std::string& name, std::string& suffix)
{
	const auto dot_pos = filename.find_last_of('.');
	if(dot_pos != std::string::npos)
	{
		name = filename.substr(0, dot_pos);
		suffix = filename.substr(dot_pos);
	}
	else
	{
		name = filename;
		suffix.clear();
	}
}

size_t GetLiteralCount(const Problem& problem)
{
	size_t cnt = 0;
	for(unsigned i = 0; i < problem.size(); ++i)
	{
		cnt += problem[i].size();
	}
	return cnt;
}

//! Размер формулы в памяти
size_t GetProblemBinarySize(const Problem& problem)
{
	return sizeof(Lit) * GetLiteralCount(problem);
}

//! Размер выделенной памяти под формулу
size_t GetProblemMemoryAlloc(const Problem& problem)
{
	size_t capacity_size = 0;
	for(unsigned i = 0; i < problem.size(); ++i)
	{
		capacity_size += problem[i].capacity();
	}
	// to do: как померять память, выделенную под вектор, содержащий эти вектора?
	return capacity_size * sizeof(Lit);
}

void FillVarOrder(const Problem& problem, VarOrder& order, const std::string& order_type)
{
	order.clear();
	//! Порядок переменных соответсвует сортировке по возрастанию их номеров
	if(order_type == "direct")
	{
		//! Построим множество номеров переменных, упорядоченных по возрастанию
		VarSet var_set;
		ExtractVarSet(problem, var_set);
		//! Максимальный номер переменной
		const VarId max_var_id = *(var_set.rbegin());
		order.resize(static_cast<size_t>(max_var_id) + 1, 0);
		unsigned index = 0;
		for(VarSet::const_iterator it = var_set.begin(); it != var_set.end(); ++it)
		{
			const VarId var_id = *it;
			order[var_id] = ++index;
		}
	}
	//! Порядок переменных определяется частотой вхождениях их литералов в формулу
	else if(order_type == "frequence")
	{
		VarCounterMap var_map;
		ExtractVarCounterMap(problem, var_map);

		const VarId max_var_id = var_map.rbegin()->first;
		order.resize(static_cast<size_t>(max_var_id) + 1, 0);
		for(VarCounterMap::const_iterator it = var_map.begin();
			it != var_map.end();
			++it)
		{
			order[it->first] = it->second;
		}
	}
	else
	{
		throw std::invalid_argument("FillVarOrder: Invalid order type argument: '" + order_type + "'.");
	}
}

ProblemType GetProblemType(const std::string& str_type)
{
	static std::map<std::string, ProblemType> problem_type_map;
	if(problem_type_map.empty())
	{
		problem_type_map.insert(std::make_pair("conflicts", ProblemTypeConflict));
		problem_type_map.insert(std::make_pair("cnf", ProblemTypeCnf));
		problem_type_map.insert(std::make_pair("dnf", ProblemTypeDnf));
	}
	std::map<std::string, ProblemType>::const_iterator it = problem_type_map.find(str_type);
	if(it == problem_type_map.end())
	{
		throw std::invalid_argument("GetProblemType: invalid problem type!");
	}
	return it->second;
}

void NegateProblem(Problem& p)
{
	//! Просто применяем по всем литералам отрицание
	//! Клиент должен помнить, также меняются функциональные связки: & -> |, | -> &
	for(unsigned i = 0; i < p.size(); ++i)
	{
		Clause& c = p[i];
		for(unsigned j = 0; j < c.size(); ++j)
		{
			c[j] ^= 1;
		}
	}
}

bool CheckKeyNotValue(const char* in_key, Options& options)
{
	const std::string key(in_key);
	if (key == "--help" || key == "-h")
	{
		options.show_help = true;
		return true;
	}

	if (key == "--version" || key == "-v")
	{
		options.show_version = true;
		return true;
	}

	if (key == "--statistic")
	{
		options.show_statistic = true;
		return true;
	}

	if (key == "--options")
	{
		options.show_options = true;
		return true;
	}

	if (key == "--run-tests")
	{
		options.run_tests = true;
		return true;
	}

	return false;
}

bool CheckKeyValue(const char* in_key, const char* in_value, Options& options)
{
	const std::string key(in_key);
	if (key == "--file" || key == "-f")
	{
		options.filename = std::string(in_value);
		return true;
	}
	if (key == "--analyze")
	{
		options.analyze = std::string(in_value);
		return true;
	}
	if (key == "--analyze-log")
	{
		options.analyze_log = std::string(in_value);
		return true;
	}
	if (key == "--analyze-var-limit")
	{
		options.analyze_var_limit = std::atol(in_value);
		return true;
	}
	if (key == "--analyze-var-fraction")
	{
		options.analyze_var_fraction = static_cast<float>(std::atof(in_value));
		return true;
	}
	if (key == "--dir")
	{
		options.dir = std::string(in_value);
		return true;
	}
	if (key == "--order")
	{
		options.order = std::string(in_value);
		return true;
	}
	if (key == "--source")
	{
		options.source = std::string(in_value);
		return true;
	}
	if (key == "--convert")
	{
		options.convert = std::string(in_value);
		return true;
	}
	if (key == "--extract-subproblem")
	{
		options.extract_subproblem = std::string(in_value);
		return true;
	}

	return false;
}

void ParseOptions(int argc, char** argv, Options& options)
{
	int i = 1;
	for (; i + 1 < argc; i += 2)
	{
		if (CheckKeyValue(argv[i], argv[i + 1], options))
		{
			continue;
		}

		if (CheckKeyNotValue(argv[i], options))
		{
			--i;
			continue;
		}

		throw std::invalid_argument("Unknown option: " + std::string(argv[i]));
	}

	// Осталась одна опция, но без значения
	if (i + 1 == argc)
	{
		// Рассмотрим возможные опции, для которых значение не нужно
		if (CheckKeyNotValue(argv[i], options))
		{
			return;
		}

		// Для других опций ожидаем значение
		const std::string msg = "Option '" + std::string(argv[i]) + "' used without value";
		throw std::invalid_argument(msg);
	}
}

void PrintOptions(std::ostream& out, const Options& options)
{
	out << "Optons:" << std::endl;
	out << "  --file [-f]            = " << options.filename << std::endl;
	out << "  --source               = " << options.source << std::endl;
	out << "  --order                = " << options.order << std::endl;
	out << "  --convert              = " << options.convert << std::endl;
	out << "  --dir                  = " << options.dir << std::endl;
	out << "  --extract-subproblem   = " << options.extract_subproblem << std::endl;
	out << "  --analyze              = " << options.analyze << std::endl;
	out << "  --analyze-log          = " << options.analyze_log << std::endl;
	out << "  --analyze-var-limit    = " << options.analyze_var_limit << std::endl;
	out << "  --analyze-var-fraction = " << options.analyze_var_fraction << std::endl;
	out << "  --options              = " << options.show_options << std::endl;
	out << "  --statistic            = " << options.show_statistic << std::endl;
	out << "  --run-tests            = " << options.run_tests << std::endl;
	out << "  --version [-v]         = " << options.show_version << std::endl;
	out << "  --help [-h]            = " << options.show_help << std::endl;
}

void PrintHelp(std::ostream& out)
{
	out << "Usage: ddiagrams [options]" << std::endl;
	out << "Available options:" << std::endl;
	out << "  --file [-f]            input data file" << std::endl;
	out << "  --source               source data type {conflicts | cnf | dnf}" << std::endl;
	out << "  --order                the order of variables used {direct | header | frequence}" << std::endl;
	out << "  --convert              " << std::endl;
	out << "  --dir                  working directory" << std::endl;
	out << "  --extract-subproblem   dump subproblem to file" << std::endl;
	out << "  --analyze              analyze structure of the problems" << std::endl;
	out << "  --analyze-log          analyze log" << std::endl;
	out << "  --analyze-var-limit    limit size of set of major variables" << std::endl;
	out << "  --analyze-var-fraction fraction size of major variables occurences in a clause" << std::endl;
	out << "  --options              print option values" << std::endl;
	out << "  --statistic            print diagram statistics" << std::endl;
	out << "  --run-tests            run all tests" << std::endl;
	out << "  --version [-v]         print current version" << std::endl;
	out << "  --help [-h]            display this reference and exit" << std::endl;
}
