﻿#pragma once

#include "Types.h"

inline double cpuTime(void) { return (double)clock() / CLOCKS_PER_SEC; }

//! Извлечь множество уникальных переменных
void ExtractVarSet(const Problem& problem, VarSet& var_set);

void ExtractVarCounterMap(const Problem& problem, VarCounterMap& var_map);

//! Минимальный и максимальный номер переменной
void GetMinMaxVars(const Problem& problem, VarId& min_var_id, VarId& max_var_id);

//! Разделить путь к папке и имя файла
void SplitFilename(const std::string& path, std::string& dir, std::string& filename);

//! Разделить имя файла на имя и суффикс
void SplitFileSuffix(const std::string& filename, std::string& name, std::string& suffix);

//! Проверяем существование файла
bool FileExists(const std::string& filename);

//! Число литералов в формуле
size_t GetLiteralCount(const Problem& problem);

//! Размер формулы в памяти
size_t GetProblemBinarySize(const Problem& problem);

//! Размер выделенной памяти под формулу
size_t GetProblemMemoryAlloc(const Problem& problem);

void FillVarOrder(const Problem& problem, VarOrder& order, const std::string& order_type);

ProblemType GetProblemType(const std::string& str_type);

//! Отрицание формулы. Применяется для перехода от КНФ конфликтных баз к ДНФ.
void NegateProblem(Problem& problem);

void ParseOptions(int argc, char** argv, Options& options);

void PrintOptions(std::ostream& out, const Options& options);

void PrintHelp(std::ostream& out);


