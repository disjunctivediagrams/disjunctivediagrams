#include "Types.h"
#include "DiagramTests.h"
#include "DisjunctiveDiagramsBuilder.h"
#include "DiagramTable.h"
#include "Utils.h"
#include "IOStream.h"

void DiagramTests::AddTest(const std::string& testfile, const std::string& diagram_log, ProblemType problem_type)
{
	tests_.push_back(testfile);
	diagram_logs_.push_back(diagram_log);
	problem_types_.push_back(problem_type);
}

void DiagramTests::RunAllTest(std::ostream& out)
{
	assert(tests_.size() == diagram_logs_.size());
	assert(tests_.size() == problem_types_.size());
	try
	{
		for(unsigned i = 0; i < tests_.size(); i++)
		{
			const std::string& filename = tests_[i];
			const std::string& etalon_file = diagram_logs_[i];

			Problem problem; 
			VarOrder order;
			try
			{
				IOStream::ReadProblem(filename, problem, order);
			}
			catch(const std::runtime_error& e)
			{
				out << "ERROR: Test #" << (i + 1) << e.what() << std::endl;
				continue;
			}

			if(problem_types_[i] != ProblemTypeDnf)
			{
				NegateProblem(problem);
			}

			DisjunctiveDiagramsBuilder builder(problem, order, problem_types_[i]);
			DisjunctiveDiagramPtr diagram = builder.BuildDiagram();

			DiagramTable diag_table(*diagram, order);

			//! ������ ������ ��������� ��� ������� �����
			DiagramTablePtr etalon_table(DiagramTable::GetFromXml(etalon_file));

			//! ���������� ��������� ����� � ��������
			if(diag_table.Equal(*etalon_table, out))
			{
				out << "Test #" << (i + 1) << ": Ok" << std::endl;
			}
			else
			{
				out << "Test #" << (i + 1) << ": Failed" << std::endl;
			}
		}
	}
	catch(const std::exception& e)
	{
		out << e.what() << std::endl;
		return;
	}
}
