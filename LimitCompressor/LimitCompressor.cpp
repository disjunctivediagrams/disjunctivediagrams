#include "stdafx.h"

std::size_t get_ascii_int(std::istream& in, std::size_t bytes);
void write_header(std::ostream& out, std::size_t var_count, std::size_t clause_count);
void write_clause(std::ostream& out, std::size_t lit_count, unsigned long long block);

int main(int argc, char* argv[])
{
	namespace po = boost::program_options;
	try
	{
		po::options_description options("Usage: TestGenerator [options]");
		options.add_options()
			("help,h", "display this help and exit")
			("version,v", "output current program version")
			("file,f", po::value<std::string>(), "input file with limits")
			("cnf,c", po::value<std::string>(), "output cnf file")
			("block,b", po::value<std::size_t>()->default_value(std::numeric_limits<size_t>::max()), "block count")
			;

		po::variables_map params;
		po::store(po::parse_command_line(argc, argv, options), params);

		if (params.count("help"))
		{
			std::cout << options;
			return 0;
		}

		if (params.count("version"))
		{
			std::cout << "Version: 0.1.0.0" << std::endl;
			std::cout << "Build date: " << __DATE__ << " " << __TIME__ << std::endl;
			return 0;
		}

		const std::string filename = params["file"].as<std::string>();
		const std::string cnf_file = params["cnf"].as<std::string>();
		const std::size_t block_count = params["block"].as<std::size_t>();

		std::ifstream in(filename.c_str(), std::ios::binary | std::ios::in);
		if(!in.is_open())
			throw std::runtime_error("main: can't open file " + filename);
		
		//! ��������� ������ 2 ����� - ������ ������ �������� �������
		const std::size_t var_count = get_ascii_int(in, 2);
		std::cout << "Var count: " << var_count << std::endl;
		
		std::ofstream out(cnf_file.c_str(), std::ios::out);
		if(!out.is_open())
			throw std::runtime_error("main: can't open file " + cnf_file);

		//! ��������� ���
		write_header(out, var_count, block_count);

		//! ������ 64-������ �����, ����������� �� � ���������, ������� ����� � ���
		unsigned long long block(0);
		for(unsigned i(0); (i < block_count) && in.good() && !in.eof(); ++i)
		{
			in.read(reinterpret_cast<char*>(&block), sizeof(block));
			//! ������ ���� ����������� � ��������
			write_clause(out, var_count, block);
		}
	}
	catch(const std::exception& e)
	{
		std::cerr << "[ERROR]: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}

std::size_t get_ascii_int(std::istream& in, std::size_t bytes)
{
	std::size_t res(0);
	char c;
	for(std::size_t i(0); i < bytes; ++i)
	{
		in.read(&c, 1);
		res = res * 10 + static_cast<std::size_t>(c - '0');
	}
	return res;
}

void write_header(std::ostream& out, std::size_t var_count, std::size_t clause_count)
{
	out << "p cnf " << var_count << " " << clause_count << std::endl;
	out << "c literals_count " << var_count * clause_count << std::endl;
	out << "c min_var_num " << 1 << std::endl;
	out << "c max_var_num " << var_count << std::endl;
	out << "c activity_order";
	for(unsigned i(1); i <= var_count; ++i)
		out << " " << i;
	out << " 0" << std::endl;
}

void write_clause(std::ostream& out, std::size_t lit_count, unsigned long long block)
{
	for(unsigned i(0); i < lit_count; ++i)
	{
		out << (block & (static_cast<unsigned long long>(1) << i) ? "-" : "");
		out << (i + 1) << " ";
	}
	out << "0\n";
}
