#include "Statistic.h"

DiagramStatistic::DiagramStatistic(const DisjunctiveDiagram& diagram)
	: avg_parent_count_(0)
	, avg_high_childs_count_(0)
	, avg_low_childs_count_(0)
	, max_parent_count_(0)
	, max_high_childs_count_(0)
	, max_low_childs_count_(0)
	, min_parent_count_(0)
	, min_high_childs_count_(0)
	, min_low_childs_count_(0)
	, vertex_count_(0)
	, leaf_count_(0)
	, root_count_(0)
	, inner_vertex_count_(0)
	, var_count_(0)
	, min_var_id_(0)
	, max_var_id_(0)
	, true_path_count_(0)
	, false_path_count_(0)
	, max_path_depth_(0)
	, memory_size_(0)
{
	//! ID ������� --> ����� ���������
	VertexInfoMap vertex_map;
	const DiagramNodes& roots = diagram.GetRoots();
	CalcParentChildCounts(roots, vertex_map);

	vertex_count_ = static_cast<unsigned>(vertex_map.size());
	for(VertexInfoMap::const_iterator it = vertex_map.begin();
		it != vertex_map.end();
		it++)
	{
		const VertexInfo& vertex_info = it->second;

		//! ������� ����������
		avg_parent_count_ += vertex_info.parent_count;
		avg_high_childs_count_ += vertex_info.high_childs_count;
		avg_low_childs_count_ += vertex_info.low_childs_count;

		//! ������������ ����������
		if(max_parent_count_ < vertex_info.parent_count)
		{
			max_parent_count_ = vertex_info.parent_count;
		}
		if(max_high_childs_count_ < vertex_info.high_childs_count)
		{
			max_high_childs_count_ = vertex_info.high_childs_count;
		}
		if(max_low_childs_count_ < vertex_info.low_childs_count)
		{
			max_low_childs_count_ = vertex_info.low_childs_count;
		}

		//! ����������� ����������
		if(min_parent_count_ > vertex_info.parent_count || it == vertex_map.begin())
		{
			min_parent_count_ = vertex_info.parent_count;
		}
		if(min_high_childs_count_ > vertex_info.high_childs_count || it == vertex_map.begin())
		{
			min_high_childs_count_ = vertex_info.high_childs_count;
		}
		if(min_low_childs_count_ > vertex_info.low_childs_count || it == vertex_map.begin())
		{
			min_low_childs_count_ = vertex_info.low_childs_count;
		}
	}
	//! ������� ��������������
	avg_parent_count_ /= vertex_count_;
	avg_high_childs_count_ /= vertex_count_;
	avg_low_childs_count_ /= vertex_count_;
}

void DiagramStatistic::CalcParentChildCounts(const DiagramNodes& roots, VertexInfoMap& vertex_map)
{
	for(DiagramNodes::const_iterator it = roots.begin();
		it != roots.end();
		it++)
	{
		const DiagramNodePtr node = *it;
		//! ��� ������������ ������ ������ �� ������
		if(node->IsLeaf())
		{
			continue;
		}
		//! ����� �������� �������
		const VertexId v_id = node->vertex_id;
		VertexInfoMap::iterator map_it = vertex_map.find(v_id);
		if(map_it == vertex_map.end())
		{
			vertex_map.insert(std::make_pair(v_id, 
				VertexInfo(0, static_cast<unsigned>(node->high_nodes.size()), static_cast<unsigned>(node->low_nodes.size()))));
		}
		else
		{
			map_it->second.high_childs_count = static_cast<unsigned>(node->high_nodes.size());
			map_it->second.low_childs_count = static_cast<unsigned>(node->low_nodes.size());
		}

		//! ����������� � ����� ������� ��������� �� 1
		IncrementParentCount(node->high_nodes, vertex_map);
		IncrementParentCount(node->low_nodes, vertex_map);

		//! ��������� ���� ����� ���������� ��� ���� ��������
		CalcParentChildCounts(node->high_nodes, vertex_map);
		CalcParentChildCounts(node->low_nodes, vertex_map);
	}
}

//
void DiagramStatistic::IncrementParentCount(const DiagramNodes& nodes, VertexInfoMap& vertex_map)
{
	for(DiagramNodes::const_iterator it = nodes.begin();
		it != nodes.end();
		it++)
	{
		const DiagramNodePtr node = *it;
		if(node->IsLeaf())
		{
			continue;
		}
		const VertexId v_id = node->vertex_id;
		VertexInfoMap::iterator map_it = vertex_map.find(v_id);
		if(map_it == vertex_map.end())
		{
			vertex_map.insert(std::make_pair(v_id, VertexInfo(1, 0, 0)));
		}
		else
		{
			map_it->second.parent_count++;
		}
	}
}

std::ostream& operator<<(std::ostream& out, const DiagramStatistic& stats)
{
	out << "Vertex count: " << stats.VertexCount() << std::endl;

	out << "Avg parents count: " << stats.AvgParentCount() << std::endl;
	out << "Avg high childs count: " << stats.AvgHighChildsCount() << std::endl;
	out << "Avg low childs count: " << stats.AvgLowChildsCount() << std::endl;
	out << "Min parents count: " << stats.MinParentCount() << std::endl;
	out << "Min high childs count: " << stats.MinHighChildsCount() << std::endl;
	out << "Min low childs count: " << stats.MinLowChildsCount() << std::endl;
	out << "Max parents count: " << stats.MaxParentCount() << std::endl;
	out << "Max high childs count: " << stats.MaxHighChildsCount() << std::endl;
	out << "Max low childs count: " << stats.MaxLowChildsCount() << std::endl;

	return out;
}

EquationSystemStatistic::EquationSystemStatistic(const EquationSystem& sys)
	: equations_count_ (0)
	, literals_count_  (0)
	, min_equation_len_(0)
	, max_equation_len_(0)
	, avg_equation_len_(0)
{
	for(EquationSystem::Equations::const_iterator eq_it = sys.equations_.begin();
		eq_it != sys.equations_.end();
		eq_it++)
	{
		const EquationSystem::Equation& eq = *eq_it;
		// ��������� eq.first
		unsigned lits_count = 1;
		const EquationSystem::VarDisj& disj = eq.second;
		for(unsigned i = 0; i < disj.size(); i++)
		{
			const EquationSystem::VarConj& conj = disj[i];
			if(conj.fake_var_id)
			{
				++lits_count;
			}
			assert(conj.node_var_id);
			++lits_count;
		}

		if(min_equation_len_ > lits_count || min_equation_len_ == 0)
		{
			min_equation_len_ = lits_count;
		}
		if(max_equation_len_ < lits_count)
		{
			max_equation_len_ = lits_count;
		}

		literals_count_ += lits_count;
		++equations_count_;
	}

	avg_equation_len_ = literals_count_ / equations_count_;
}

std::ostream& operator<<(std::ostream& out, const EquationSystemStatistic& stats)
{
	out << "Equations count: " << stats.EquationCount() << std::endl;
	out << "Literals count: " << stats.LiteralCount() << std::endl;
	out << "Min equation length: " << stats.MinEquationLength() << std::endl;
	out << "Max equation length: " << stats.MaxEquationLength() << std::endl;
	out << "Avg equation length: " << stats.AvgEquationLength() << std::endl;
	return out;
}
