﻿#ifndef MINISAT_LOGGER_H
#define MINISAT_LOGGER_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <algorithm>

#include "minisat/mtl/Vec.h"
#include "minisat/core/SolverTypes.h"

bool IsEmpty(const Minisat::vec<Minisat::CRef>& clauses, const Minisat::ClauseAllocator& ca);

struct VarActivity
{
	VarActivity(Minisat::Var v, double a): var(v), activity(a) {}

	Minisat::Var var;
	double activity;
	bool operator<(const VarActivity& v) const { return activity < v.activity; }
};



typedef std::set<VarActivity> VarActivitySet;
typedef VarActivitySet::const_iterator VarActivitySetIter;

//! Выгрузить базу конфликтных дизъюнктов в файл
void WriteLearntClauses(
	const Minisat::vec<Minisat::CRef>& learnts, 
	const Minisat::ClauseAllocator& ca, 
	const Minisat::VMap<double>& activity,
	const std::string& path);

#endif // MINISAT_LOGGER_H
