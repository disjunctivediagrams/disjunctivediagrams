﻿#include "MinisatLogger.h"

bool IsEmpty(const Minisat::vec<Minisat::CRef>& clauses, const Minisat::ClauseAllocator& ca)
{
	for(int i = 0; i < clauses.size(); i++)
	{
		if(ca[clauses[i]].size() > 0)
			return false;
	}
	return true;
}



/* Выгрузить базу конфликтных дизъюнктов в файл
   Заголовок файла:
   p cnf <var_count> <disjunct_count>
   с literals_count <count>
   c min_var_num <num>
   c max_var_num <num>
   c activity_order <var1> ... <varN>
*/
void WriteLearntClauses(
	const Minisat::vec<Minisat::CRef>& learnts, 
	const Minisat::ClauseAllocator& ca, 
	const Minisat::VMap<double>& activity,
	const std::string& path)
{
	if(IsEmpty(learnts, ca))
		return;

	static int file_id = 0;
	int lit_count = 0;
	int min_var_num = 0;
	int max_var_num = 0;
	int var_count = 0;
	int disjunct_count = 0;
	std::set<Minisat::Var> var_set;

	std::stringstream ss_filename;
	ss_filename << path << "LearntsClauses_" << ++file_id << ".cnf";
	std::ofstream out(ss_filename.str().c_str(), std::ios::out);
	if(out.is_open())
	{
		//! конфликтные дизъюнкты
		for(int i = 0; i < learnts.size(); i++)
		{
			const Minisat::Clause& c = ca[learnts[i]];
			for(int j = 0; j < c.size(); j++)
			{
				const Minisat::Lit& lit = c[j];
				//! Поскольку внутри решателя нумерация переменных выполняется с 0, то увеличиваем номер на 1
				Minisat::Var variable = Minisat::var(lit) + 1;

				//! Запомнить максимальный и минимальный номера переменных
				if(max_var_num < variable)
					max_var_num = variable;
				if(min_var_num > variable || min_var_num == 0)
					min_var_num = variable;

				//! Сохраняем только уникальные элементы
				var_set.insert(variable);
			}
			lit_count += c.size();
		}
		
		std::vector<VarActivity> var_vec;
		for(std::set<Minisat::Var>::const_iterator it = var_set.begin(); 
			it != var_set.end(); 
			++it)
		{
			const Minisat::Var var_id = *it;
			var_vec.push_back(VarActivity(var_id, activity[var_id - 1]));
		}

		std::sort(var_vec.begin(), var_vec.end());
		var_count = var_vec.size();
		disjunct_count = learnts.size();

		out << "p cnf " << var_count << " " << disjunct_count << std::endl;
		out << "c literals_count " << lit_count << std::endl;
		out << "c min_var_num " << min_var_num << std::endl;
		out << "c max_var_num " << max_var_num << std::endl;
		out << "c activity_order";
		for(std::vector<VarActivity>::const_iterator var_it = var_vec.begin(); 
			var_it != var_vec.end(); 
			++var_it)
		{
			out << " " << var_it->var;
		}
		out << " 0\n";

		//! Выводим формулу
		for(int i = 0; i < learnts.size(); i++)
		{
			const Minisat::Clause& c = ca[learnts[i]];
			for(int j = 0; j < c.size(); j++)
			{
				const Minisat::Lit& lit = c[j];
				//! Поскольку внутри решателя нумерация переменных выполняется с 0, то увеличиваем номер на 1
				Minisat::Var variable = Minisat::var(lit) + 1;
				out << (Minisat::sign(lit) ? "-" : "") << variable << " ";
			}
			out << "0\n";
		}

		out.close();
	}
}
