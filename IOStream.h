﻿#ifndef IO_STREAM_H
#define IO_STREAM_H

#include "Types.h"
#include "Statistic.h"

namespace IOStream
{

//! Вывод в файл журнала
class Log
{
public:
	
	Log(const std::string& filename);
	Log(const char* filename);
	~Log();

	void WriteOut(const char* msg);
	void WriteOut(const std::string& msg);
	void WriteOut(const Problem& problem);
	void WriteOut(const VarOrder& order);
	void WriteOut(const EquationSystem& sys);
	void WriteOut(const DiagramStatistic& stat);
	void WriteOut(const EquationSystemStatistic& stat);

private:
	std::ofstream out_;
};

//! Читаем формулу из файла
void ReadProblem(const std::string& filename, Problem& problem, VarOrder& order);

//! Прочитать содержимое текстового файла в массив строк
void ReadLinesFromFile(const std::string& filename, std::vector<std::string>& lines);

} // namespace IOStream

#endif // IO_STREAM_H
