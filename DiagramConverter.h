﻿#ifndef DIAGRAM_CONVERTER_H
#define DIAGRAM_CONVERTER_H

#include "Types.h"

class DiagramConverter
{
public:
	DiagramConverter();

	//! Преобразование диаграммы в КНФ
	void Convert(const DisjunctiveDiagram& diagram, Problem& cnf);
	//! Преобразование диаграммы в систему уравнений
	void Convert(const DisjunctiveDiagram& diagram, EquationSystem& sys);

	~DiagramConverter();

private:
	//! Проход по дереву от корня к листьям
	void WriteTransitions(Problem& problem, const DiagramNodePtr root, VarId markup_var_id);
	
	VarId NextMarkupVarId();

	void AddParents(const DiagramNodes& nodes, const DiagramNodePtr parent, EquationSystem& sys);

private:

	//! Записать один переход между узлами диаграммы в КНФ: u1 & var <--> u2
	//! u1, u2 - переменные разметки (фиктивные переменные)
	typedef void (*WriteTransitionProcedure)(Problem&, VarId, VarId, VarId, bool);

	WriteTransitionProcedure WriteTransition;

	//! Введем переменную для терминального узла (единицы)
	VarId leaf_var_id_;
	//! Максимальный номер переменной
	//! Используется при создании новых переменных разметки
	VarId max_var_id_;

	std::unordered_set<DiagramNodePtr, DiagramNodeHash> visited_nodes_;
};

#endif // DIAGRAM_CONVERTER_H
