﻿#include "DisjunctiveDiagramsBuilder.h"

DisjunctiveDiagramsBuilder::DisjunctiveDiagramsBuilder(Problem& dnf
													   , const VarOrder& order
													   , ProblemType problem_type)
	: problem_    (dnf)
	, order_      (order)
	, lit_less_   (order)
	, clause_less_(order)
	, problem_type_(problem_type)
	, diagram_node_less_()
	, hash_()
{
}

DisjunctiveDiagramsBuilder::~DisjunctiveDiagramsBuilder()
{
}

DisjunctiveDiagramPtr DisjunctiveDiagramsBuilder::BuildDiagram()
{
	diagram_ = std::make_shared<DisjunctiveDiagram>();
	diagram_->problem_type_ = problem_type_;

	ProblemRange ranges;
	//! Отсортируем литералы в дизъюктах относительно порядка order_
	for(unsigned idx = 0; idx < problem_.size(); idx++)
	{
		Clause& clause = problem_[idx];
		Clause::iterator it_begin = clause.begin();
		Clause::iterator it_end = clause.end();
		std::sort(it_begin, it_end, lit_less_);
		ranges.push_back(std::make_pair(it_begin, it_end));
	}
	// fixme: возможны случае, когда некоторые корни склеятся с др. поддеревьями
	DiagramNodesSet root_set = BuildDiagramNodes(ranges);
	diagram_->roots_.insert(diagram_->roots_.begin(), root_set.begin(), root_set.end());

	//! Проставим тип узла
	for(DiagramNodes::iterator root_it = diagram_->roots_.begin();
		root_it != diagram_->roots_.end();
		++root_it)
	{
		(*root_it)->node_type = RootNode;
	}

	//! Нумерация вершин диаграммы
	EnumerateDiagramNodes();

	return diagram_;
}

DiagramNodesSet DisjunctiveDiagramsBuilder::BuildDiagramNodes(ProblemRange& ranges)
{
	//! Определим множество уникальных переменных в текущем фрагменте
	std::set<VarId> var_set;
	for(unsigned i = 0; i < ranges.size(); ++i)
	{
		ClauseRange& range = ranges[i];
		var_set.insert(*(range.first) >> 1);
	}

	//! Здесь храним возвращаемый результат
	DiagramNodesSet nodes;

	for(std::set<VarId>::const_iterator var_it = var_set.begin();
		var_it != var_set.end();
		++var_it)
	{
		const VarId var_id = *var_it;
		ProblemRange high_range;
		ProblemRange low_range;
		bool has_high_terminal = false;
		bool has_low_terminal = false;

		//! Заполняем high_range и low_range
		for(unsigned i = 0; i < ranges.size(); i++)
		{
			ClauseRange& range = ranges[i];
			const Lit lit = *(range.first);
			if(var_id == (lit >> 1))
			{
				const bool phase = ((lit & 1) == 0);
				if(phase)
				{
					if(has_high_terminal)
					{
						continue;
					}
					if(range.first + 1 == range.second)
					{
						has_high_terminal = true;
						high_range.clear();
						continue;
					}
					high_range.push_back(std::make_pair(range.first + 1, range.second));
				}
				else
				{
					if(has_low_terminal)
					{
						continue;
					}
					if(range.first + 1 == range.second)
					{
						has_low_terminal = true;
						low_range.clear();
						continue;
					}
					low_range.push_back(std::make_pair(range.first + 1, range.second));
				}
			}
		}

		//! Строим high-потомки
		DiagramNodesSet high_nodes;
		if(has_high_terminal)
		{
			high_nodes.insert(diagram_->GetTrueLeaf());
		}
		else if(!high_range.empty())
		{
			high_nodes = BuildDiagramNodes(high_range);	
		}
		//! Если high-потомков нет совсем, то добавляем ссылку на терминальный ноль
		if(high_nodes.empty())
		{
			high_nodes.insert(diagram_->GetFalseLeaf());
		}

		//! Строим low-потомки
		DiagramNodesSet low_nodes;
		if(has_low_terminal)
		{
			low_nodes.insert(diagram_->GetTrueLeaf());
		}
		else if(!low_range.empty())
		{
			low_nodes = BuildDiagramNodes(low_range);
		}
		//! Если low-потомков нет совсем, то добавляем ссылку на терминальный ноль
		if(low_nodes.empty())
		{
			low_nodes.insert(diagram_->GetFalseLeaf());
		}

		//! Создаем новый узел диаграммы
		DiagramNodePtr node(new DiagramNode(InternalNode, var_id, high_nodes, low_nodes));
		node = AddDiagramNode(node);
		nodes.insert(node);

		// fixme: нужно рассмотреть случай, когда единственный low-потомок узла совпадает с его единственным high-потомком
		// такие узлы должны удаляться, а новым родителем потом становится родитель удаляемого узла

		//! Для статистики
		//! Переменных относительно не много, поэтому не должно влиять на потребление памяти
		diagram_->var_set_.insert(var_id);
	}

	return nodes;
}

DiagramNodePtr DisjunctiveDiagramsBuilder::AddDiagramNode(DiagramNodePtr node)
{
	//! Если такой узел уже есть, то склеиваем их
	DiagramHashTable::iterator it_node = diagram_->table_.find(node);
	if(it_node != diagram_->table_.end())
	{
		//! Все дублирующие узлы, кроме листьев удаляем
		if(!node->IsLeaf() && (*it_node != node))
		{
			delete node;
		}
		return *it_node;
	}
	//! Такого узла в диаграмме еще нет - добавляем в таблицу
	node->hash_key = hash_(node);
	diagram_->table_.insert(node);
	return node;
}

void DisjunctiveDiagramsBuilder::EnumerateDiagramNodes()
{
	unsigned vertex_id = 0;
	//! Перенумеруем все вершины, кроме терминальных
	for(DiagramHashTable::iterator it = diagram_->table_.begin(); it != diagram_->table_.end(); it++)
	{
		if(!(*it)->IsLeaf())
		{
			(*it)->vertex_id = ++vertex_id;
		}
	}
}


