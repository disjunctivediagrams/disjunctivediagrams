#include "SplitUtil.h"
#include "../Types.h"
#include "../DimacsParser.h"


void SortProblem(Problem& p);
void WriteOutProblem(const std::string& path, Problem::const_iterator it_begin, Problem::const_iterator it_end);

int main(int argc, char* argv[])
{
	namespace po = boost::program_options;
	
	try
	{
		//! ����� ��������� ������
		po::options_description options("Usage: SplitUtil [options]");
		options.add_options()
			("help,h", "display this help and exit")
			("input-dir,i", po::value<std::string>()->default_value(""), "")
			("output-dir,o", po::value<std::string>()->default_value(""), "")
			("file,f", po::value<std::string>()->default_value(""), "input data file")
			("level,l", po::value<std::string>()->default_value("0"), "split level")
			("version,v", "output current program version")
			;

		po::variables_map params;
		po::store(po::parse_command_line(argc, argv, options), params);

		if (params.count("help"))
		{
			std::cout << options;
			return 0;
		}

		if (params.count("version"))
		{
			std::cout << "Version: 1.0.0.0" << std::endl;
			std::cout << "Build date: " << __DATE__ << " " << __TIME__ << std::endl;
		}

		const std::string input_file = params["file"].as<std::string>();
		const std::string input_dir = params["input-dir"].as<std::string>();
		const std::string output_dir = params["output-dir"].as<std::string>();
		const unsigned split_level = params["level"].as<unsigned>();
		const std::string filename = input_dir + input_file;

		if(!file_exists(filename))
		{
			throw std::runtime_error(std::string("File '") + filename + std::string("' not exists!"));
		}

		std::ifstream file(filename.c_str(), std::ios::in);
		if(!file.is_open())
		{
			throw std::runtime_error(std::string("Can't open file ") + filename);
		}

		IStream in(file);
		Problem problem;
		VarOrder order;

		//! ������ �������� ������� �� �����
		if(!parseDimacs(in, problem, order))
		{
			file.close();
			throw std::runtime_error("Parse the input file is failed!");
		}
		file.close();

		//! ��������� ������ � ���������
		SortProblem(problem);

		unsigned tree_counter = 0;
		Problem::const_iterator it_start;
		Problem::const_iterator it_stop = problem.begin();
		while(it_stop != problem.end())
		{
			it_start = it_stop;
			const Clause& c1 = *it_start;
			while(++it_stop != problem.end())
			{
				
				const Clause& c2 = *it_stop;
				const VarId var1 = c1.front() >> 1;
				const VarId var2 = c2.front() >> 1;
				if(var1 != var2)
				{
					break;
				}
			}
			//! ���������� ���������� �������� ������� � ��������� ����
			std::stringstream ss;
			ss << output_dir << "tree_" << ++tree_counter << ".cnf";
			WriteOutProblem(ss.str(), it_start, it_stop);
		}

	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << std::endl;
		return 1;
	}
	catch(...)
	{
		std::cerr << "Unknown exception" << std::endl;
		return 1;
	}

	return 0;
}

inline bool ClauseLessByLits(const Lit& lit1, const Lit& lit2)
{
	return lit1 < lit2;
}

//! ��������� ���������� ������ �� ������� ��������
//! ��������������, ��� �������� � ���������� ��� �������������
inline bool ProblemLessByClauses(const Clause& c1, const Clause& c2)
{
	assert(c1.size() && c2.size());
	return c1.front() < c2.front();
}

void SortProblem(Problem& p)
{
	for(unsigned i = 0; i < p.size(); ++i)
	{
		Clause& c = p[i];
		std::sort(c.begin(), c.end(), ClauseLessByLits);
	}

	std::sort(p.begin(), p.end(), ProblemLessByClauses);
}

void WriteOutProblem(const std::string& path, Problem::const_iterator it_begin, Problem::const_iterator it_end)
{
	std::ofstream out(path.c_str(), std::ios::out);
	if(out.is_open())
	{
		//! ��������� ��� ���������� ��� ���������
		{
			std::set<VarId> var_set;
			size_t lit_count = 0;
			size_t clause_count = 0;
			Problem::const_iterator it = it_begin;
			while(it != it_end)
			{
				const Clause& c = *it;
				lit_count += c.size();
				for(unsigned j = 0; j < c.size(); j++)
				{
					const Lit& lit = c[j];
					var_set.insert(static_cast<VarId>(lit >> 1));
				}
				++clause_count;
				++it;
			}

			out << "p cnf " << var_set.size() << " " << clause_count << std::endl;
			out << "c literals_count " << lit_count << std::endl;
			out << "c min_var_num " << *(var_set.begin()) << std::endl;
			out << "c max_var_num " << *(var_set.rbegin()) << std::endl;
		}

		//! ����� � ���� ���� �������
		Problem::const_iterator it = it_begin;
		while(it != it_end)
		{
			const Clause& c = *it;
			for(unsigned i = 0; i < c.size(); ++i)
			{
				const Lit& lit = c[i];
				const VarId var_id = (lit >> 1);
				out << (lit & 1 ? "-" : "") << var_id << " ";
			}
			out << "0\n";
			++it;
		}

		out.close();
	}
}
