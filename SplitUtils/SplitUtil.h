#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <ctime>
#include <vector>
#include <map>
#include <set>
#include <string>
#include <algorithm>
#include <cassert>
#include <exception>
#include <ctime>

#include <boost/shared_ptr.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>

static inline bool file_exists(const std::string& filename)
{
	std::fstream fin(filename.c_str(), std::ios::in);
	if(fin.is_open())
	{
		fin.close();
		return true;
	}
	return false;
}

inline double cpuTime(void) { return (double)clock() / CLOCKS_PER_SEC; }
