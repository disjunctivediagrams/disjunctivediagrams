#ifndef DIAGRAM_TABLE_H
#define DIAGRAM_TABLE_H

#include "Types.h"

/*****************************************************************
 Формат xml-файла для выгрузки диаграммы:

 <?xml version="1.0" encoding="utf-8"?>
 <DisjunctiveDiagram>
	<VarOrder count="" min_var_id="" max_var_id="">
		<Var id="1" order="1"/>
		...
	</VarOrder>
	<Roots count="">
		<Root vertex_id=""/>
		...
	</Roots>
	<Vertices count="">
		<Vertex id="1" var_id="1"/>
			<High>
				<Child vertex_id=""/>
				...
				<Terminal value="true"/>
			</High>
			

			<Low>
				<Child vertex_id=""/>
				...
				<Terminal value="false"/>
			</Low>
		</Vertex>
		...
	</Vertices>
 </DisjunctiveDiagram>
*****************************************************************/

class DiagramTable;
typedef std::shared_ptr<DiagramTable> DiagramTablePtr;

class DiagramTable
{
public:
	DiagramTable(const DisjunctiveDiagram& diagram, const VarOrder& var_order);

	static DiagramTablePtr GetFromXml(const std::string& xml_file);
	void SaveToXml(const std::string& xml_file) const;

	bool Equal(const DiagramTable& table) const;
	bool Equal(const DiagramTable& table, std::ostream& out) const;
	
public:

	typedef std::vector<VertexId> VertexIdContainer;

	struct VertexChilds
	{
		VertexChilds(): terminal(false){}

		VertexIdContainer childs;
		//! terminal имеет смысл, если childs.size() == 0
		bool terminal;
		bool operator==(const VertexChilds& c) const
		{
			return (terminal == c.terminal && childs == c.childs);
		}
	};

	struct Vertex
	{
		Vertex(VertexId vertex_id_ = 0, VarId var_id_ = 0)
			: vertex_id(vertex_id_), var_id(var_id_) 
		{}

		VertexId vertex_id;
		VarId var_id;
		VertexChilds high;
		VertexChilds low;

		bool operator==(const Vertex& v) const
		{
			return (vertex_id == v.vertex_id && var_id == v.var_id && high == v.high && low == v.low);
		}
	};
	typedef std::vector<Vertex> VertexContainer;

	//! Для упорядочивания вершин
	struct CompareVertex
	{
		bool operator()(const Vertex& a, const Vertex& b) const
		{
			return a.vertex_id < b.vertex_id;
		}
	};

private:

	DiagramTable(){}

	void SortCollections();

	//! Порядок переменных
	VarOrder var_order_;
	//! Предполагается сортировка по возрастанию
	VertexIdContainer roots_;
	//! Предполагается сортировка по возрастанию по vertex_id
	VertexContainer vertices_;
	
	VarId min_var_id_;
	VarId max_var_id_;
	size_t var_count_;
};

#endif // DIAGRAM_TABLE_H
