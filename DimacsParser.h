﻿
#include "Types.h"

//входной поток - чтение КНФ
class IStream
{
	enum { buffer_size = 64*1024 };
public:
	IStream(std::istream& s = std::cin)
		: in(s)
		, pos_(0)
		, size_(0)
	{
		buffer_ = new char[buffer_size];
		assureLookahead();
	}
	~IStream()
	{
		delete[] buffer_;
	}
	char operator * () {return (pos_ < size_) ? buffer_[pos_] : 0;}
	void operator ++ () {pos_++; assureLookahead();}
	char operator[](int index) const { return (pos_ + index < size_ ? buffer_[pos_ + index] : 0);}
	unsigned  position() const {return pos_;}
	//! Проверяем, что и в буфере и в потоке больше нет данных
	bool eof() const {return (pos_ >= size_) && in.eof();}

private:
	void assureLookahead()
	{
		if(pos_ >= size_)
		{
			in.read(buffer_, buffer_size);
			size_ = static_cast<unsigned>(in.gcount());
			pos_ = 0;
		}
	}

	std::istream& in;
	char* buffer_;
	//! позиция текущего символа в буфере
	unsigned pos_;
	//! число символов в буфере (size_ <= buffer_size)
	unsigned size_;
};

///////////////////////////////////////////////////////////////////////////////////////////////////
// Функции разбора DIMACS-файла

template<typename B>
bool parseDimacs(B& in, Problem& dnf, VarOrder& order)
{
	unsigned min_var_num = 0;
	unsigned max_var_num = 0;
	unsigned lit_count = 0;
	for(;;)
	{
		skipWhitespace(in);
		if(in.eof()) break;
		//! Заголовок DIMACS-файла
		else if((*in == 'p') && (match(in, "p cnf") || match(in, "p dnf")))
		{
			if(match(in, "p cnf"))
			{
				skipString(in, static_cast<int>(std::string("p cnf").size()));
			}
			else
			{
				skipString(in, static_cast<int>(std::string("p dnf").size()));
			}
			int vars    = parseInt(in);
            int clauses = parseInt(in);
			//for(int i = 0; i < vars; i++) newVar();
			skipLine(in);
		}
		//! Число литералов в формуле
		else if((*in == 'c') && match(in, "c literals_count"))
		{
			skipString(in, static_cast<int>(std::string("c literals_count").size()));
			lit_count = abs(parseInt(in));
			skipLine(in);
		}
		//! Минимальный номер переменной
		else if((*in == 'c') && match(in, "c min_var_num"))
		{
			skipString(in, static_cast<int>(std::string("c min_var_num").size()));
			min_var_num = abs(parseInt(in));
			skipLine(in);
		}
		//! Максимальный номер переменной
		else if((*in == 'c') && match(in, "c max_var_num"))
		{
			skipString(in, static_cast<int>(std::string("c max_var_num").size()));
			max_var_num = abs(parseInt(in));
			order.resize(max_var_num + 1);
			skipLine(in);
		}
		//! Порядок переменных (по их активности)
		else if((*in == 'c') && match(in, "c activity_order"))
		{
			skipString(in, static_cast<int>(std::string("c activity_order").size()));
			unsigned index = 0;
			for(;;)
			{
				VarId var = abs(parseInt(in));
				if(var == 0) break;
				order[var] = ++index;
			}
		}
		//! Пропускаем комментации
		else if(*in == 'c')
			skipLine(in);
		else{
			Clause cl;
			readClause(in, cl);
			if(cl.size())
				dnf.push_back(cl);
		}
	}
	return true;
}

template<typename B>
void skipWhitespace(B& in)
{
	while ((*in >= 9 && *in <= 13) || *in == 32)
	{
        ++in; 
		if(in.eof()) 
			return;
	}
}

template<typename B>
void skipLine(B& in)
{
	while(in.eof() || *in != '\n') ++in;
	if(*in == '\n') ++in;
}

template<typename B>
static bool match(B& in, const char* str) {
    int i;
    for (i = 0; (in[i] != 0) && (str[i] != '\0'); i++)
        if (in[i] != str[i])
            return false;

	return (in[i] != 0 ? true : false);
}

template<typename B>
static void skipString(B& in, int count)
{
	while(count > 0)
	{
		++in;
		--count;
	}
}

template<typename B>
int parseInt(B& in)
{
	int     val = 0;
    bool    neg = false;
    skipWhitespace(in);
    if      (*in == '-') neg = true, ++in;
    else if (*in == '+') ++in;
	if (*in < '0' || *in > '9')
	{
		std::cout << "PARSE ERROR! Unexpected char:" << *in << std::endl; 
		exit(1);
	}
    while (*in >= '0' && *in <= '9')
        val = val*10 + (*in - '0'),
        ++in;
    return neg ? -val : val;
}

template<typename B>
void readClause(B& in, Clause& cl)
{
	int var, lit;
	//Vec<Lit> lits;
	for(;;)
	{
		lit = parseInt(in);
		if(lit == 0) break;
		var = abs(lit); // переменные нумеруются с 1
		cl.push_back( 2*var + ((lit > 0) ? 0 : 1) );

		//var = abs(lit) - 1;//переменные нумеруются с нуля
        //while (var >= nVars()) newVar();
        //lits.push( (lit > 0) ? Lit(var) : ~ Lit(var) );
	}
	//addClause(lits);
}
