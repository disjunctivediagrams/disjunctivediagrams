﻿#include "stdafx.h"
#include "Types.h"

unsigned DiagramNode::constructors_ = 0;
unsigned DiagramNode::destructors_ = 0;

DiagramNode::DiagramNode(DiagramNodeType type)
	: var_id    (0)
	, markup_var_id(0)
	, vertex_id (0)
	, node_type (type)
	, hash_key  (0)
	, high_nodes()
	, low_nodes ()
{
	constructors_++;
}

DiagramNode::~DiagramNode()
{
	destructors_++;
}

DiagramNode::DiagramNode(DiagramNodeType type, VarId var, const DiagramNodesSet& high, const DiagramNodesSet& low)
	: var_id     (var)
	, markup_var_id(0)
	, vertex_id  (0)
	, hash_key   (0)
	, node_type  (type)
{
	high_nodes.insert(high_nodes.begin(), high.begin(), high.end());
	low_nodes.insert(low_nodes.begin(), low.begin(), low.end());
	constructors_++;
}

DiagramNode::DiagramNode(DiagramNodeType type, VarId var)
	: var_id     (var)
	, markup_var_id(0)
	, vertex_id  (0)
	, hash_key   (0)
	, node_type  (type)
{
	constructors_++;
}

bool DiagramNode::Equals(const DiagramNode& node) const
{
	if(Value() != node.Value())
	{
		return false;
	}
	if((high_nodes.size() != node.high_nodes.size()) ||
		low_nodes.size() != node.low_nodes.size())
	{
		return false;
	}
	//! Проверяем на равенство правое поддерево
	for(DiagramNodes::const_iterator 
		left = high_nodes.begin(), left_end = high_nodes.end(),
		right = node.high_nodes.begin(), right_end = node.high_nodes.end();
		left != left_end && right != right_end;
		left++, right++)
	{
		if(!(*left)->Equals(**right))
		{
			return false;
		}
	}
	
	//! Проверяем на равенство левое поддерево
	for(DiagramNodes::const_iterator
		left = low_nodes.begin(), left_end = low_nodes.end(),
		right = node.low_nodes.begin(), right_end = node.low_nodes.end();
		left != left_end && right != right_end;
		left++, right++)
	{
		if(!(*left)->Equals(**right))
		{
			return false;
		}
	}
	//! Узлы и растущие из них деревья равны
	return true;
}

size_t DiagramNode::Size() const
{
	//return sizeof(DiagramNodeType) + 2 * sizeof(VarId) + sizeof(VertexId) + sizeof(HashKey) 
	//	+ sizeof(DiagramNodePtr) * (high_nodes.size() + low_nodes.size());
	return sizeof(VarId) + sizeof(DiagramNodePtr) * (high_nodes.size() + low_nodes.size());
}

DiagramNode DisjunctiveDiagram::true_leaf_ = DiagramNode(TrueNode);
DiagramNode DisjunctiveDiagram::false_leaf_ = DiagramNode(FalseNode);

DisjunctiveDiagram::DisjunctiveDiagram()
	: variable_count_(0)
	, true_path_count_(0)
	, false_path_count_(0)
	, max_path_depth_(0)
	, duplicate_reduced_(0)
	, vertex_reduced_(0)
	, min_var_num_(0)
	, max_var_num_(0)
	, root_join_cnt_(0)
{
	//! Сразу добавляем в хеш-таблицу терминальные вершины 
	table_.insert(&false_leaf_);
	table_.insert(&true_leaf_);
}

DisjunctiveDiagram::~DisjunctiveDiagram()
{
	for(DiagramHashTable::iterator it = table_.begin(); it != table_.end(); ++it)
	{
		DiagramNodePtr node = *it;
		if(!node->IsLeaf())
		{
			delete node;
		}
	}
}

size_t DisjunctiveDiagram::DiagramSize() const
{
	size_t size = 0;
	for(DiagramHashTable::const_iterator it = table_.begin(); it != table_.end(); it++)
	{
		size += (*it)->Size();
	}
	//size += (roots_.size() + 2) * sizeof(DiagramNodePtr);
	//size += var_set_.size() * sizeof(VarId);
	//size += 9 * sizeof(size_t);
	return size;
}

HashKey DiagramNodeHash::operator()(const DiagramNodePtr& node) const
{
	HashKey seed = 0;
	hash_comb(seed, node->Value());
	const DiagramNodes& high = node->high_nodes;
	for(DiagramNodes::const_iterator it = high.begin(), it_end = high.end(); 
		it != it_end;
		it++)
	{
		hash_comb(seed, (*it)->hash_key);
	}
	const DiagramNodes& low = node->low_nodes;
	for(DiagramNodes::const_iterator it = low.begin(), it_end = low.end(); 
		it != it_end;
		it++)
	{
		hash_comb(seed, (*it)->hash_key);
	}
	return seed;
}

bool DiagramNodeLess::operator()(const DiagramNodePtr& node_1, const DiagramNodePtr& node_2) const
{
	return node_1->Value() < node_2->Value();
}

bool DiagramNodeEquals::operator()(const DiagramNodePtr& node_1, const DiagramNodePtr& node_2) const
{
	return node_1->Equals(*node_2);
}

Options::Options()
	: analyze_log("result.txt")
	, analyze_var_limit(20)
	, analyze_var_fraction(0.5f)
	, dir("./")
	, source("conflicts")
	, order("header")
{}
