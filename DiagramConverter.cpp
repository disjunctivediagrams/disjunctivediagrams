﻿#include "DiagramConverter.h"

//! Записать один переход между узлами диаграммы в КНФ
//! u1, u2 - переменные разметки (фиктивные переменные)
static void WriteEquivalenceTransition(Problem& problem, VarId var_id, VarId u1_id, VarId u2_id, bool var_phase);
static void WriteImplicationTransition(Problem& problem, VarId var_id, VarId u1_id, VarId u2_id, bool var_phase);

DiagramConverter::DiagramConverter()
	: max_var_id_(0)
	, leaf_var_id_(0)
	, WriteTransition(0)
{
}

DiagramConverter::~DiagramConverter()
{}

void DiagramConverter::Convert(const DisjunctiveDiagram &diagram, Problem& problem)
{
	//! Идентификатор для терминальной вершины
	leaf_var_id_ = diagram.MaxVarId() + 1;
	max_var_id_ = leaf_var_id_;

	//! Выбор преобразования переходов диаграммы в КНФ
	if(diagram.GetProblemType() == ProblemTypeConflict || diagram.GetProblemType() == ProblemTypeDnf)
	{
		// std::cout << "DiagramConverter: convert diagram to CNF using implication transition" << std::endl;
		WriteTransition = WriteImplicationTransition;
	}
	else
	{
		// std::cout << "DiagramConverter: convert diagram to CNF using equivalence transition" << std::endl;
		WriteTransition = WriteEquivalenceTransition;
	}

	//! Проходим все пути в диаграмме, начиная с корневых вершин (они не имеют родителей)
	const DiagramNodes& roots = diagram.GetRoots();
	for(DiagramNodes::const_iterator it = roots.begin(), it_end = roots.end();
		it != it_end;
		it++)
	{
		const DiagramNodePtr& root_ptr = *it;
		WriteTransitions(problem, root_ptr, 0);
	}

	//! В конец добавляем однолитеральный дизъюнкт - номер терминальной вершины (единица)
	const unsigned phase = (diagram.GetProblemType() == ProblemTypeCnf || diagram.GetProblemType() == ProblemTypeConflict ? 1 : 0);
	Clause c(1);
	c[0] = Lit((leaf_var_id_ << 1) ^ phase);
	problem.push_back(c);
}

void DiagramConverter::WriteTransitions(Problem& problem, const DiagramNodePtr node, VarId markup_var_id)
{
	// Сразу помечаем узел как посещенный
	visited_nodes_.insert(node);

	const VarId& var_id = node->var_id;
	const DiagramNodes& high_childs = node->high_nodes;
	//! Переходы из родителя в high-потомков
	for(DiagramNodes::const_iterator it = high_childs.begin();
		it != high_childs.end();
		it++)
	{
		const DiagramNodePtr& child_ptr = *it;
		//! Потомок - терминальный ноль
		if(child_ptr->node_type == FalseNode)
		{
			//! Ничего не делаем
			continue;
		}
		//! Потомок - терминальная единица
		else if(child_ptr->node_type == TrueNode)
		{
			//! Добавляем ограничение в КНФ
			WriteTransition(problem, var_id, markup_var_id, leaf_var_id_, false);
		}
		//! Потомок - внутренняя вершина диаграммы
		else
		{
			if(child_ptr->markup_var_id == 0)
			{
				child_ptr->markup_var_id = NextMarkupVarId();
			}
			//const VarId next_markup_var_id = child_ptr->markup_var_id ? child_ptr->markup_var_id : NextMarkupVarId();
			//const VarId next_markup_var_id = max_var_id_ + child_ptr->vertex_id;
			WriteTransition(problem, var_id, markup_var_id, child_ptr->markup_var_id, false);
			
			// Рекурсивно обработаем узел-ребенок, если еще не сделали этого
			if (visited_nodes_.find(child_ptr) == visited_nodes_.end())
			{
				WriteTransitions(problem, child_ptr, child_ptr->markup_var_id);
			}
		}
	}

	const DiagramNodes& low_childs = node->low_nodes;
	//! Переходы из родителя в low-потомков
	for(DiagramNodes::const_iterator it = low_childs.begin();
		it != low_childs.end();
		it++)
	{
		const DiagramNodePtr& child_ptr = *it;
		//! Потомок - терминальный ноль
		if(child_ptr->node_type == FalseNode)
		{
			//! Ничего не делаем
			continue;
		}
		//! Потомок - терминальная единица
		else if(child_ptr->node_type == TrueNode)
		{
			//! Добавляем ограничение в КНФ
			WriteTransition(problem, var_id, markup_var_id, leaf_var_id_, true);
		}
		//! Потомок - внутренняя вершина диаграммы
		else
		{
			if(child_ptr->markup_var_id == 0)
			{
				child_ptr->markup_var_id = NextMarkupVarId();
			}
			//const VarId next_markup_var_id = NextMarkupVarId();
			//const VarId next_markup_var_id = max_var_id_ + child_ptr->vertex_id;
			WriteTransition(problem, var_id, markup_var_id, child_ptr->markup_var_id, true);
			// Рекурсивно обработаем узел-ребенок, если еще не сделали этого
			if (visited_nodes_.find(child_ptr) == visited_nodes_.end())
			{
				WriteTransitions(problem, child_ptr, child_ptr->markup_var_id);
			}
		}
	}
}

VarId DiagramConverter::NextMarkupVarId()
{
	return ++max_var_id_;
}

// fixme: реализация неправильная
//! Для случая эквивалентности
static void WriteEquivalenceTransition(Problem& problem, VarId var_id, VarId u1_id, VarId u2_id, bool var_phase)
{
	const unsigned phase = (var_phase ? 1 : 0);
	if(u1_id)
	{
		//! Переход в диаграмме: u1 & var <--> u2
		//! Соответствующий фрагмент КНФ: (!u1 | !var | u2)&(u1 | !u2)&(var | !u2)
		Clause c1(3), c2(2), c3(2);

		c1[0] = Lit((u1_id << 1) ^ 1); // !u1
		c1[1] = Lit((var_id << 1) ^ phase ^ 1); // !var
		c1[2] = Lit(u2_id << 1); // u2

		c2[0] = Lit(u1_id << 1); // u1
		c2[1] = Lit((u2_id << 1) ^ 1); // !u2

		c3[0] = Lit((var_id << 1) ^ phase); // var
		c3[1] = Lit((u2_id << 1) ^ 1); // !u2

		problem.push_back(c1);
		problem.push_back(c2);
		problem.push_back(c3);
	}
	else
	{
		//! Переход в диаграмме: var <--> u1
		//! Соответствующий фрагмент КНФ: (var | !u1)&(!var | u1)
		Clause c1(2), c2(2);
		c1[0] = Lit((var_id << 1) ^ phase); // var
		c1[1] = Lit((u2_id << 1) ^ 1); // !u2

		c2[0] = Lit((var_id << 1) ^ phase ^ 1); // !var
		c2[1] = Lit(u2_id << 1); // u2

		problem.push_back(c1);
		problem.push_back(c2);
	}
}

//! Для случая импликации (преобразование конфликтных баз)
static void WriteImplicationTransition(Problem& problem, VarId var_id, VarId u1_id, VarId u2_id, bool var_phase)
{
	const unsigned phase = (var_phase ? 1 : 0);
	if(u1_id)
	{
		//! Переход в диаграмме: u1 & var --> u2
		//! Соответствующий фрагмент КНФ: (!u1 | !var | u2)
		Clause c(3);
		c[0] = Lit((u1_id << 1) ^ 1); // !u1
		c[1] = Lit((var_id << 1) ^ phase ^ 1); // !var
		c[2] = Lit(u2_id << 1); // u2
		problem.push_back(c);
	}
	else
	{
		//! Переход в диаграмме: var --> u2
		//! Соответствующий фрагмент КНФ: (!var | u2)
		Clause c(2);
		c[0] = Lit((var_id << 1) ^ phase ^ 1); // !var
		c[1] = Lit(u2_id << 1); // u2
		problem.push_back(c);
	}
}

void DiagramConverter::Convert(const DisjunctiveDiagram& diagram, EquationSystem& sys)
{
	sys.equations_.clear();
	//! Идентификатор для терминальной вершины
	max_var_id_ = leaf_var_id_ = diagram.MaxVarId() + 1;

	//! Создание переменных разметки, для которых будем определять уравнения
	const DiagramHashTable& table = diagram.GetTable();
	//! Уравнение, описывающее все переходы в терминальную единицу
	sys.equations_.insert(EquationSystem::Equation(leaf_var_id_, EquationSystem::VarDisj()));
	for(DiagramHashTable::const_iterator node_it = table.begin();
		node_it != table.end();
		node_it++)
	{
		const DiagramNodePtr node = *node_it;
		if(node->IsLeaf())
		{
			node->markup_var_id = (node->node_type == TrueNode ? leaf_var_id_ : 0);
			std::cout << "Leaf var_id: " << node->var_id << std::endl;
			continue;
		}
		if(node->IsRoot())
		{
			node->markup_var_id = 0;
			//std::cout << "Root var_id: " << node->var_id << std::endl;
			continue;
		}
		node->markup_var_id = NextMarkupVarId();
		sys.equations_.insert(EquationSystem::Equation(node->markup_var_id, EquationSystem::VarDisj()));
	}

	for(DiagramHashTable::const_iterator node_it = table.begin();
		node_it != table.end();
		node_it++)
	{
		const DiagramNodePtr node = *node_it;
		if(node->IsLeaf())
		{
			continue;
		}

		//! Обрабатываем потомков
		AddParents(node->high_nodes, node, sys);
		AddParents(node->low_nodes, node, sys);
	}
}

void DiagramConverter::AddParents(const DiagramNodes& nodes, const DiagramNodePtr parent, EquationSystem& sys)
{
	for(DiagramNodes::const_iterator node_it = nodes.begin();
		node_it != nodes.end();
		node_it++)
	{
		const DiagramNodePtr node = *node_it;
		const VarId markup_var_id = node->markup_var_id;
		//! такое возможно только для терминального нуля 
		if(markup_var_id == 0)
		{
			//assert(node->node_type == FalseNode);
			if(node->node_type != FalseNode)
			{
			std::cerr << "var id: " << node->var_id << ", markup var id: " << node->markup_var_id 
				<< ", parent var id: " << parent->var_id << ", parent markup var id: " << parent->markup_var_id << std::endl;
			}
			continue;
		}
		EquationSystem::Equations::iterator eq_it = sys.equations_.find(markup_var_id);
		if(eq_it == sys.equations_.end())
		{
			std::cerr << "Error: markup_var_id = " << markup_var_id << " not found." << std::endl;
			continue;
		}
		eq_it->second.push_back(EquationSystem::VarConj(parent->var_id, parent->markup_var_id));
	}
}
