﻿#include "Types.h"
#include "IOStream.h"
#include "DisjunctiveDiagramsBuilder.h"
#include "DiagramConverter.h"
#include "Statistic.h"
#include "DiagramTable.h"
#include "DiagramTests.h"
#include "AnalyzeUtils.h"
#include "Utils.h"

int main(int argc, char** argv)
{
	try
	{
		Options options;
		ParseOptions(argc, argv, options);

		// Показать справку
		if (options.show_help)
		{
			PrintHelp(std::cout);
			return 0;
		}

		// Показать текущую версию
		if (options.show_version)
		{
			std::cout << "Version: 1.0.1.0" << std::endl;
			std::cout << "Build date: " << __DATE__ << " " << __TIME__ << std::endl;
		}

		// Показать текущие значения опций
		if (options.show_options)
		{
			PrintOptions(std::cout, options);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////
		// case 1

		if(options.run_tests)
		{
			//! Прогнать на тестовых примерах и выйти
			DiagramTests tests;
			tests.AddTest("Tests/test1.cnf", "Tests/diagram_table_test1.xml", ProblemTypeCnf);
			tests.AddTest("Tests/test2.cnf", "Tests/diagram_table_test2.xml", ProblemTypeCnf);
			tests.AddTest("Tests/test3.cnf", "Tests/diagram_table_test3.xml", ProblemTypeCnf);
			tests.RunAllTest(std::cout);
			return 0;
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////
		// case 2

		if(!options.analyze.empty())
		{
			std::vector<std::string> files;
			IOStream::ReadLinesFromFile(options.analyze, files);
			ProblemAnalyzer analyzer(options.analyze_var_limit, options.analyze_var_fraction, options.analyze_log);
			analyzer.Analyze(files);
			return 0;
		}

		if (!options.extract_subproblem.empty())
		{
			const std::string source_filename = options.filename;
			const std::string result_filename = options.extract_subproblem;
			const size_t var_set_size = options.analyze_var_limit;
			const float var_occurence = options.analyze_var_fraction;
			const std::string log_file = options.analyze_log;
			ProblemAnalyzer analyzer(var_set_size, var_occurence, log_file);
			
			// Чтение исходной формулы
			VarOrder order;
			Problem source_problem;
			IOStream::ReadProblem(source_filename, source_problem, order);

			// Извлечение подформулы
			Problem result_problem;
			std::string info;
			analyzer.SelectMajorSubset(source_problem, result_problem, info);

			// Сохранить КНФ в файл
			IOStream::Log log(result_filename);
			log.WriteOut(result_problem);
			return 0;
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////
		// case 3: Строим диаграмму по блобу префиксов, выводим статистику и по итогу строим КНФ

		/////////////////////////////////////////////////////////////////////////////////////////////////
		// case 4

		const std::string dir = options.dir;
		const std::string filename = options.filename;
		const std::string source_type = options.source;
		const std::string order_type = options.order;
		const std::string diagram_table_filename = options.table;
		const std::string convert_keys = options.convert;

		if(!FileExists(filename))
		{
			throw std::runtime_error(std::string("File '") + filename + std::string("' not exists!"));
		}

		Problem problem;
		VarOrder order;
		IOStream::ReadProblem(filename, problem, order);

		//! Порядок переменных задан в заголовке входного файла (используется по умолчанию)
		if(order_type != "header")
		{
			FillVarOrder(problem, order, order_type);
		}
		if(order.empty())
		{
			throw std::runtime_error("The collection of variable order is empty!");
		}

		{
			//! Журнализируем текущий порядок переменных
			IOStream::Log log("Logs/order.log");
			log.WriteOut(order);
		}

		//! Строим отрицание считанной формулы (КНФ => ДНФ)
		if(source_type == "conflicts" || source_type == "cnf")
		{
			NegateProblem(problem);
		}
			
		//! Строим диаграмму
		double build_time = cpuTime();
		DisjunctiveDiagramsBuilder builder(problem, order, GetProblemType(source_type));
		DisjunctiveDiagramPtr diagram = builder.BuildDiagram();
		build_time = cpuTime() - build_time;

		//! Бинарный размер диаграммы и исходной формулы
		const size_t diagram_bin_size = diagram->DiagramSize();
		const size_t problem_bin_size = GetProblemBinarySize(problem);
		const size_t literal_count = GetLiteralCount(problem);

		std::cout << "Diagram build time: " << build_time << " s." << std::endl;
		std::cout << "Variable count: " << diagram->VariableCount() << std::endl;
		std::cout << "Problem lit count: " << literal_count << std::endl;
		std::cout << "Diagram vertex count: " << diagram->VertexCount() << std::endl;
		std::cout << "Lit/Vertex ratio: " << static_cast<double>(GetLiteralCount(problem)) / diagram->VertexCount() << std::endl;
		std::cout << "Problem size (bytes): " << problem_bin_size << std::endl;
		std::cout << "Diagram size (bytes): " << diagram_bin_size << std::endl;
		std::cout << "Problem/Diagram size ratio: " << static_cast<double>(problem_bin_size) / diagram_bin_size << std::endl;
		std::cout << "DiagramNode constructors: " << DiagramNode::constructors_ << std::endl;
		std::cout << "DiagramNode destructors: " << DiagramNode::destructors_ << std::endl;

		//! Сохранить диаграмму в xml-файле
		if(!diagram_table_filename.empty())
		{
			const std::string xml_filename = std::string("Tests/") + diagram_table_filename + std::string(".xml");
			DiagramTable table(*diagram, order);
			table.SaveToXml(xml_filename);
		}

		//! Статистика по диаграмме
		if(options.show_statistic)
		{
			DiagramStatistic diagram_stats(*diagram);
			IOStream::Log log("Logs/diagram_stat.log");
			log.WriteOut(diagram_stats);
		}
		
		//! Преобразование диаграммы в формулы
		//! Тип формул задается ключами
		if(!convert_keys.empty())
		{
			DiagramConverter converter;

			//! Построенная диаграмма преобразуется в КНФ
			if(convert_keys.find('c') != std::string::npos)
			{
				Problem cnf;
				converter.Convert(*diagram, cnf);
				
				std::cout << "Converted problem size (bytes): " << GetProblemBinarySize(cnf) << std::endl;
				std::cout << "Converted problem lit count: " << GetLiteralCount(cnf) << std::endl;

				//! Сохранить КНФ в файл
				IOStream::Log log("Logs/diagram.cnf");
				log.WriteOut(cnf);
			}
			
			//! Построенная диаграмма преобразуется в систему уравнений
			if(convert_keys.find('e') != std::string::npos)
			{
				EquationSystem sys;
				converter.Convert(*diagram, sys);
				EquationSystemStatistic equation_stats(sys);
				std::cout << equation_stats << std::endl;
				//! Записать систему уравнений в файл
				IOStream::Log log("Logs/equation_system.txt");
				log.WriteOut(sys);
			}
		}

	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << std::endl;
		return 1;
	}
	catch(...)
	{
		std::cerr << "Unknown exception" << std::endl;
		return 1;
	}

	return 0;
}
