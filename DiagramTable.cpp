#include "DiagramTable.h"
//#include <QtCore/QFile>
//#include <QtXml/QtXml>
//#include <QtXml/QtXml>

DiagramTable::DiagramTable(const DisjunctiveDiagram& diagram, const VarOrder& var_order)
	: var_order_(var_order)
	, min_var_id_(diagram.MinVarId())
	, max_var_id_(diagram.MaxVarId())
	, var_count_(diagram.VariableCount())
{
	const DiagramHashTable& table = diagram.GetTable();
	for(DiagramHashTable::const_iterator it = table.begin();
		it != table.end();
		it++)
	{
		const DiagramNode& node = **it;
		if(node.IsLeaf())
		{
			continue;
		}
		Vertex vertex(node.vertex_id, node.var_id);
		
		if(node.high_nodes.empty() || node.low_nodes.empty())
		{
			throw std::runtime_error("DiagramTable::DiagramTable: empty child list!");
		}
		
		const DiagramNode& first_high = **(node.high_nodes.begin());
		if(node.high_nodes.size() == 1 && first_high.IsLeaf())
		{
			vertex.high.terminal = (first_high.node_type == TrueNode);
		}
		else
		{
			for(DiagramNodes::const_iterator it = node.high_nodes.begin();
				it != node.high_nodes.end();
				it++)
			{
				const DiagramNode& child = **it;
				vertex.high.childs.push_back(child.vertex_id);
			}
		}
		
		const DiagramNode& first_low = **(node.low_nodes.begin());
		if(node.low_nodes.size() == 1 && first_low.IsLeaf())
		{
			vertex.low.terminal = (first_low.node_type == TrueNode);
		}
		else
		{
			for(DiagramNodes::const_iterator it = node.low_nodes.begin();
				it != node.low_nodes.end();
				it++)
			{
				const DiagramNode& child = **it;
				vertex.low.childs.push_back(child.vertex_id);
			}
		}

		vertices_.push_back(vertex);
	}

	const DiagramNodes& roots = diagram.GetRoots();
	for(DiagramNodes::const_iterator it = roots.begin();
		it != roots.end();
		it++)
	{
		const DiagramNode& node = **it;
		roots_.push_back(node.vertex_id);
	}

	//! Упорядочиваем коллекции
	SortCollections();
}

bool DiagramTable::Equal(const DiagramTable& table) const
{
	return (var_order_ == table.var_order_ && roots_ == table.roots_ && vertices_ == table.vertices_ && 
		var_count_ == table.var_count_ && min_var_id_ == table.min_var_id_ && max_var_id_ == table.max_var_id_);
}

bool DiagramTable::Equal(const DiagramTable& table, std::ostream& out) const
{
	if(var_order_ != table.var_order_)
	{
		out << "DiagramTable::Equal: VarOrder not equals." << std::endl;
		return false;
	}
	if(roots_ != table.roots_)
	{
		out << "DiagramTable::Equal: Roots not equals." << std::endl;
		return false;
	}
	if(vertices_ != table.vertices_)
	{
		out << "DiagramTable::Equal: Vertices not equals." << std::endl;
		return false;
	}
	if(var_count_ != table.var_count_)
	{
		out << "DiagramTable::Equal: var_count not equals." << std::endl;
		return false;
	}
	if(min_var_id_ != table.min_var_id_)
	{
		out << "DiagramTable::Equal: min_var_id not equals." << std::endl;
		return false;
	}
	if(max_var_id_ != table.max_var_id_)
	{
		out << "DiagramTable::Equal: max_var_id not equals." << std::endl;
		return false;
	}
	return true;
}

/*namespace
{
void XmlSerializeVarOrder(QXmlStreamWriter& xml, const VarOrder& order, unsigned var_count, unsigned min_var_id, unsigned max_var_id)
{
	xml.writeStartElement("VarOrder");
	//! В текущей реализации в общем случаем order.size() != var_count_
	xml.writeAttribute("count", QString::number(var_count));
	xml.writeAttribute("min_var_id", QString::number(min_var_id));
	xml.writeAttribute("max_var_id", QString::number(max_var_id));
	unsigned var_id = 0;
	for(VarOrder::const_iterator it = order.begin();
		it != order.end();
		it++, var_id++)
	{
		const VarOrder::value_type& value = *it;
		if(value > 0)
		{
			xml.writeStartElement("Var");
			xml.writeAttribute("id", QString::number(var_id));
			xml.writeAttribute("order", QString::number(value));
			xml.writeEndElement();
		}
	}
	xml.writeEndElement();
}
void XmlSerializeRoots(QXmlStreamWriter& xml, const DiagramTable::VertexIdContainer& vertex_ids)
{
	xml.writeStartElement("Roots");
	xml.writeAttribute("count", QString::number(vertex_ids.size()));
	for(unsigned i = 0; i < vertex_ids.size(); i++)
	{
		const VertexId vertex_id = vertex_ids[i];
		xml.writeStartElement("Root");
		xml.writeAttribute("vertex_id", QString::number(vertex_id));
		xml.writeEndElement();
	}
	xml.writeEndElement();
}
void XmlSerializeVertexChilds(QXmlStreamWriter& xml, const DiagramTable::VertexChilds& vc)
{
	if(vc.childs.empty())
	{
		xml.writeStartElement("Terminal");
		xml.writeAttribute("value", (vc.terminal ? "true" : "false"));
		xml.writeEndElement();
	}
	else
	{
		for(unsigned i = 0; i < vc.childs.size(); i++)
		{
			const VertexId vertex_id = vc.childs[i];
			xml.writeStartElement("Child");
			xml.writeAttribute("vertex_id", QString::number(vertex_id));
			xml.writeEndElement();
		}
	}
}

void XmlSerializeVertices(QXmlStreamWriter& xml, const DiagramTable::VertexContainer& vertices)
{
	xml.writeStartElement("Vertices");
	xml.writeAttribute("count", QString::number(vertices.size()));
	for(DiagramTable::VertexContainer::const_iterator it = vertices.begin();
		it != vertices.end();
		it++)
	{
		const DiagramTable::Vertex& v = *it;
		xml.writeStartElement("Vertex");
		xml.writeAttribute("id", QString::number(v.vertex_id));
		xml.writeAttribute("var_id", QString::number(v.var_id));
		//! High-потомки
		xml.writeStartElement("High");
		XmlSerializeVertexChilds(xml, v.high);
		xml.writeEndElement();
		//! Low-потомки
		xml.writeStartElement("Low");
		XmlSerializeVertexChilds(xml, v.low);
		xml.writeEndElement();
		xml.writeEndElement();
	}
	xml.writeEndElement();
}

} // namespace

void DiagramTable::SaveToXml(const std::string& filename) const
{
	QFile file(filename.c_str());
	if(!file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
	{
		throw std::runtime_error(std::string("Can't open file ") + filename);
	}

	QXmlStreamWriter xml(&file);
	xml.setCodec("UTF-8");
	xml.setAutoFormatting(true);
	xml.writeStartDocument();
	xml.writeStartElement("DisjunctiveDiagram");

	//! Выводим порядок переменных
	XmlSerializeVarOrder(xml, var_order_, var_count_, min_var_id_, max_var_id_);
	//! Выводим номера корневых вершин
	XmlSerializeRoots(xml, roots_);
	//! Выводим список всех вершин диаграммы
	XmlSerializeVertices(xml, vertices_);

	xml.writeEndElement();
	xml.writeEndDocument();
}

namespace
{
void ReadVarOrder(QXmlStreamReader& xml, VarOrder& order, unsigned& var_count, unsigned& min_var_id, unsigned& max_var_id)
{
	if(xml.isStartElement() && xml.name() == "VarOrder")
	{
		QXmlStreamAttributes var_order_attr = xml.attributes();
		var_count = var_order_attr.value("count").toString().toUInt();
		min_var_id = var_order_attr.value("min_var_id").toString().toUInt();
		max_var_id = var_order_attr.value("max_var_id").toString().toUInt();
		const size_t order_size = max_var_id + 1;
		order.resize(order_size);
		while(!(xml.isEndElement() && xml.name() == "VarOrder") && !xml.hasError())
		{
			xml.readNext();
			if(xml.isStartElement() && xml.name() == "Var" && !xml.hasError())
			{
				QXmlStreamAttributes var_attr = xml.attributes();
				const VarId var_id = static_cast<VarId>(var_attr.value("id").toString().toInt());
				const unsigned index = static_cast<unsigned>(var_attr.value("order").toString().toInt());
				if(static_cast<size_t>(var_id) >= order.size())
				{
					throw std::out_of_range("ReadVarOrder: out of range order container!");
				}
				order[var_id] = index;
			}
		}
	}
}
void ReadRoots(QXmlStreamReader& xml, DiagramTable::VertexIdContainer& roots)
{
	if(xml.isStartElement() && xml.name() == "Roots")
	{
		QXmlStreamAttributes attr = xml.attributes();
		roots.reserve(attr.value("count").toString().toInt());
		while(!(xml.isEndElement() && xml.name() == "Roots") && !xml.hasError())
		{
			xml.readNext();
			if(xml.isStartElement() && xml.name() == "Root" && !xml.hasError())
			{
				QXmlStreamAttributes node_attr = xml.attributes();
				const VertexId vertex_id = static_cast<VertexId>(node_attr.value("vertex_id").toString().toInt());
				roots.push_back(vertex_id);
			}
		}
	}
}
void ReadVertexChilds(QXmlStreamReader& xml, DiagramTable::VertexChilds& childs)
{
	const QString tag_name = xml.name().toString();
	while(!(xml.isEndElement() && xml.name() == tag_name) && !xml.hasError())
	{
		xml.readNext();
		if(xml.isStartElement() && xml.name() == "Child" && !xml.hasError())
		{
			QXmlStreamAttributes attr = xml.attributes();
			const VertexId vertex_id = static_cast<VertexId>(attr.value("vertex_id").toString().toInt());
			childs.childs.push_back(vertex_id);
		}
		if(xml.isStartElement() && xml.name() == "Terminal" && !xml.hasError())
		{
			assert(childs.childs.empty());
			QXmlStreamAttributes attr = xml.attributes();
			childs.terminal = (attr.value("value").toString() == "true" ? true : false);
		}
	}

}
void ReadVertices(QXmlStreamReader& xml, DiagramTable::VertexContainer& vertices)
{
	if(xml.isStartElement() && xml.name() == "Vertices")
	{
		QXmlStreamAttributes attr = xml.attributes();
		vertices.reserve(attr.value("count").toString().toInt());
		while(!(xml.isEndElement() && xml.name() == "Vertices") && !xml.hasError())
		{
			xml.readNext();
			if(xml.isStartElement() && xml.name() == "Vertex" && !xml.hasError())
			{
				QXmlStreamAttributes va = xml.attributes();
				const VertexId vertex_id = static_cast<VertexId>(va.value("id").toString().toInt());
				const VarId var_id = static_cast<VarId>(va.value("var_id").toString().toInt());
				DiagramTable::Vertex v(vertex_id, var_id);
				while(!(xml.isEndElement() && xml.name() == "Vertex") && !xml.hasError())
				{
					xml.readNext();
					if(xml.isStartElement() && xml.name() == "High" && !xml.hasError())
					{
						ReadVertexChilds(xml, v.high);
					}
					else if(xml.isStartElement() && xml.name() == "Low" && !xml.hasError())
					{
						ReadVertexChilds(xml, v.low);
					}
				}
				vertices.push_back(v);
			}
		}
	}
}
} // namespace

DiagramTablePtr DiagramTable::GetFromXml(const std::string& filename)
{
	DiagramTablePtr table(new DiagramTable);

	QFile file(filename.c_str());
	if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		throw std::runtime_error(std::string("Can't open file ") + filename);
	}

	QXmlStreamReader xml(&file);

	while(!xml.atEnd() && !xml.hasError())
	{
		xml.readNext();
		if(xml.isStartElement() && !xml.hasError())
		{
			if(xml.name() == "VarOrder")
			{
				ReadVarOrder(xml, table->var_order_, table->var_count_, table->min_var_id_, table->max_var_id_);
			}
			else if(xml.name() == "Roots")
			{
				ReadRoots(xml, table->roots_);
			}
			else if(xml.name() == "Vertices")
			{
				ReadVertices(xml, table->vertices_);
			}
		}
	}
	if(xml.hasError())
	{
		throw std::runtime_error(std::string("DiagramTable::GetFromXml: ") + xml.errorString().toStdString());
	}

	//! Упорядочиваем коллекции
	table->SortCollections();

	return table;
}*/

// fixme: пока заглушка, нужно переделать без использования Qt, чтобы убрать зависимости
DiagramTablePtr DiagramTable::GetFromXml(const std::string& xml_file)
{
	throw std::runtime_error("DiagramTable::GetFromXml is not implemented");
	return nullptr;
}

// fixme: пока заглушка, нужно переделать без использования Qt, чтобы убрать зависимости
void DiagramTable::SaveToXml(const std::string& xml_file) const
{
	throw std::runtime_error("DiagramTable::SaveToXml is not implemented");
}

void DiagramTable::SortCollections()
{
	std::sort(roots_.begin(), roots_.end());
	CompareVertex comparator;
	std::sort(vertices_.begin(), vertices_.end(), comparator);
	for(VertexContainer::iterator it = vertices_.begin();
		it != vertices_.end();
		it++)
	{
		Vertex& v = *it;
		std::sort(v.high.childs.begin(), v.high.childs.end());
		std::sort(v.low.childs.begin(), v.low.childs.end());
	}
}
